This is Cyklo-navigation application for iPhone. It is client of [Cykloplanner](http://www.cykloplanovac.cz) project of Czech Technical University.

Cykloplanner is dedicated to planning journeys in several Czech cities such as Praha, Brno, Plzeň. Planning is restricted to cities due to interesting complexity path finding.

From XCode 7 you should be able to run on your device without any setup. Open Cykloplanovac.xcworkspace. It joins several toolkit together.