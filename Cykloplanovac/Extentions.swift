//
//  Extentions.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 25/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation

extension String {
    public func urlEncode() -> String {
        //var customAllowedSet =  NSCharacterSet(charactersInString:"!@#$%&*'();:=+,/?[]").invertedSet
        let customAllowedSet =  NSCharacterSet(charactersInString:"=\"#%/<>?@\\^`{|}").invertedSet
        if let escapedString = self.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet) {
            return escapedString
        }
        return self
    }
}

extension UIViewController {
    func presentAlert(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title:NSLocalizedString("OK", comment: "alert button title"), style: UIAlertActionStyle.Cancel) { (alertAction:UIAlertAction) -> Void in
        }
        alertController.addAction(action)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
