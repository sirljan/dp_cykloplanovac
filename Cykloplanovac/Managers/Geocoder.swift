//
//  Geocoder.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 22/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreLocation

class Geocoder {
    
    static func geocodeLocation(locationCoordinate:CLLocationCoordinate2D?, completion:(address:String?, error:NSError?) -> Void) {
        if let locationCoordinate = locationCoordinate {
            let geocoder = CLGeocoder()
            let location = CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in
                if let error = error {
                    print("Reverse geocode failed with error \(error) for location \(locationCoordinate)")
                    completion(address: nil, error: error)
                }
                if let placemark = placemarks?[0] {
                    var address = ""
                    if let street = placemark.thoroughfare {
                        address.appendContentsOf(street)
                        if let streetNumber = placemark.subThoroughfare {
                            address.appendContentsOf(", " + streetNumber)
                        }
                        if let locality = placemark.locality {
                            address.appendContentsOf(", " + locality)
                        }
                    }
                    completion(address: address, error: nil)
                }
            }
        } else {
            completion(address: nil, error: nil)
        }
    }
    
    static func geocodeAddressString(address:String, userCoordinate:CLLocationCoordinate2D?, completion:(coordinate: CLLocationCoordinate2D?, error:NSError?) -> Void) {
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address) { (placemarks, error) -> Void in
            if let error = error {
                print("Geocode failed for address \"\(address)\" with error: \(error)")
                completion(coordinate: nil, error: error)
            }
            else if let placemarks = placemarks {
                var closestPlacemarek:CLPlacemark? = placemarks.first
                var minDistance = DBL_MAX
                for placemark in placemarks {
                    if let placemarkLocation = placemark.location {
                        if let userCoordinate = userCoordinate {
                            let userLocation = CLLocation(latitude: userCoordinate.latitude, longitude: userCoordinate.longitude)
                            if placemarkLocation.distanceFromLocation(userLocation) < minDistance {
                                minDistance = placemarkLocation.distanceFromLocation(userLocation)
                                closestPlacemarek = placemark
                            }
                        }
                    }
                }
                print(closestPlacemarek?.location?.coordinate)
                completion(coordinate: closestPlacemarek?.location?.coordinate, error: nil)
            }
            else {
                let errorDescription = "No retrieved placemarks."
                let userInfo = [
                    NSLocalizedDescriptionKey : (NSLocalizedString(errorDescription, comment: "error description"))
                ]
                let error = NSError(domain: "Geocoding", code: 0, userInfo: userInfo)
                print("ERROR: ",errorDescription)
                completion(coordinate: nil, error: error)
            }
        }
    }
}