//
//  BackendCommunicator.swift
//  Cykloplanovac
//
//  Created by Jenda on 26/07/15.
//  Copyright (c) 2015 FEL CVUT. All rights reserved.
//

import Foundation

let apiV2URL:String = "http://transport.felk.cvut.cz/cycle-planner/api/v2/journeys"
let apiV3URL:String = "http://its.felk.cvut.cz/cycle-planner-1.5.0-SNAPSHOT/api/v3/planner/plan"

let feedBackURI:String = "http://transport.felk.cvut.cz/cycle-planner-1.1.3-SNAPSHOT/bicycleJourneyPlanning/feedback"

let debug = false
let debugNetwork = true

class BackendCommunicator {
    
    static let sharedInstance = BackendCommunicator()
    
    private init() {
    }
    
    internal func planJourney(journey:Journey, completionHandler: (locationURL:NSURL?, errorMessage:String?) -> Void) {
        let jsonData = try? NSJSONSerialization.dataWithJSONObject(journey.dictionaryRepresentation(), options: NSJSONWritingOptions.PrettyPrinted)
        let jsonString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        if debugNetwork {print("sending: \(jsonString)")}
        
        if let URL = NSURL.init(string: apiV3URL) {
            let request = NSMutableURLRequest.init(URL: URL)
            request.HTTPMethod = "POST"
            request.addValue("application/json;charset=UTF-8", forHTTPHeaderField:"Content-Type")
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField:"Accept")
            request.addValue("UTF-8", forHTTPHeaderField:"Encoding")
            request.HTTPBody = jsonData
            
            let session = NSURLSession.sharedSession()
            let postDataTask = session.dataTaskWithRequest(request, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                var datastring:NSString?
                if let responseData = data  {
                    datastring = NSString(data: responseData, encoding: NSUTF8StringEncoding)
                }
                if (error == nil) {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if httpResponse.statusCode == 201 {
                            let headerDictionary:Dictionary = httpResponse.allHeaderFields
                            let headerLoc = "Location"
                            if let location = headerDictionary[headerLoc] as? String {
                                completionHandler(locationURL: NSURL(string: location), errorMessage: nil)
                            } else {
                                let errorMessage = NSLocalizedString("Response from server doesn't contain header '\(headerLoc)'", comment: "error message")
                                completionHandler(locationURL: nil, errorMessage: errorMessage)
                            }
                        } else {
                            let errorMessage = NSLocalizedString("Error code: \(httpResponse.statusCode)", comment: "error message")
                            print(errorMessage)
                            completionHandler(locationURL: nil, errorMessage: errorMessage)
                            if datastring != nil { print("response data: \(datastring!)")}
                        }
                    }
                }
                else
                {
                    let errorMessage = NSLocalizedString("Planning journey failed. Error code: \(error!.code) \nerror: \(error!.localizedDescription)", comment: "error text")
                    print(errorMessage)
                    completionHandler(locationURL: nil, errorMessage: errorMessage)
                }
            })
            postDataTask.resume()
        }
    }
    
    internal func getJourneyAtLocationURL(locationURL: NSURL?, completionHandler:(dictionary:NSDictionary?, errorMessage:String?)->Void) {
        if let URL = locationURL {
            let request = NSMutableURLRequest.init(URL: URL)
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField:"Accept")
            let session = NSURLSession.sharedSession()
            let getDataTask = session.dataTaskWithRequest(request, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                if error == nil {
                    if let JSONData = data  {
                        let JSONDictionary = (try? NSJSONSerialization.JSONObjectWithData(JSONData, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary
                        if debugNetwork {print("recieving: \(JSONDictionary)")}
                        completionHandler(dictionary: JSONDictionary, errorMessage: nil)
                    } else {
                        let errorMessage = NSLocalizedString("Getting journey failed. Error code: \(error!.code) \nerror: \(error!.localizedDescription)", comment: "error text")
                        completionHandler(dictionary: nil, errorMessage: errorMessage)
                    }
                }
                else
                {
                    print("ERROR: \(error)")
                    let errorMessage = NSLocalizedString("Getting journey failed. Error code: \(error!.code) \nerror: \(error!.localizedDescription)", comment: "error text")
                    completionHandler(dictionary: nil, errorMessage: errorMessage)
                }
            })
            getDataTask.resume()
        }
    }
    
    internal func postFeedBack(track:Track, completion:(errorMessage:String?) -> Void) {
        let jsonData = try? NSJSONSerialization.dataWithJSONObject(track.dictionaryRepresentation(), options: NSJSONWritingOptions.PrettyPrinted)
        let jsonString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        if debugNetwork {print("sending: \(jsonString)")}
        
        if let URL = NSURL.init(string: feedBackURI) {
            let request = NSMutableURLRequest.init(URL: URL)
            request.HTTPMethod = "POST"
            request.addValue("application/json;charset=UTF-8", forHTTPHeaderField:"Content-Type")
            request.addValue("application/json, text/plain, */*", forHTTPHeaderField:"Accept")
            request.addValue("UTF-8", forHTTPHeaderField:"Encoding")
            request.HTTPBody = jsonData
            
            let session = NSURLSession.sharedSession()
            let postDataTask = session.dataTaskWithRequest(request, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                var datastring:NSString?
                if let responseData = data  {
                    datastring = NSString(data: responseData, encoding: NSUTF8StringEncoding)
                    print("response data: ",datastring)
                }
                if error != nil
                {
                    let errorMessage = NSLocalizedString("Sending feedback failed. Error code: \(error!.code) \nerror: \(error!.localizedDescription)", comment: "error text")
                    print(errorMessage)
                    completion(errorMessage: errorMessage)
                } else {
                    completion(errorMessage: nil)
                }
            })
            postDataTask.resume()
        }
    }
    
}