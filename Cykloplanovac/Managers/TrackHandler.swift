//
//  TrackHandler.swift
//  Cykloplanovac
//
//  Created by Jenda on 09/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class TrackHandler {
    var historyTuples:[(time:NSDate, distance:Float)] = []
    weak var closestStep:Step?
    weak var plan:Plan?
    let track:Track
    let historyTreshold:NSTimeInterval
    let offTrackDegree:Float
    let percentOffTrackRecords:Float
    var trackingFinished:Bool = false
    
    init(plan:Plan?, user:User, historyTreshold:NSTimeInterval = 10.0, offTrackDegree:Float = 0.001 /*0.0003*/, percentOffTrackRecords:Float = 0.5) {

        let track = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(Track), inManagedObjectContext: DataController.sharedInstance.managedObjectContext) as! Track
        track.journey = plan?.journey
        track.plan = plan
        track.user = user
        track.trackPoints = NSOrderedSet()
        track.trackDate = NSDate()
        track.distance = NSNumber(double: 0)
        self.track = track
        
        self.plan = plan
        self.historyTreshold = historyTreshold
        self.offTrackDegree = offTrackDegree
        self.percentOffTrackRecords = percentOffTrackRecords
    }
    
    func distanceFromLocation(
        location                    P:CLLocationCoordinate2D,
        toLineSegmentWithLocation   A:CLLocationCoordinate2D,
        andLocation                 B:CLLocationCoordinate2D) -> Double {
            if ((P.longitude - A.longitude) * (B.longitude - A.longitude) +
                (P.latitude - A.latitude) * (B.latitude - A.latitude) < 0) {
                    let pow1 = pow((P.longitude - A.longitude), 2)
                    let pow2 = pow((P.latitude - A.latitude),2)
                    return sqrt(pow1 + pow2)
            }
            if ((P.longitude - B.longitude) * (A.longitude - B.longitude) +
                (P.latitude - B.latitude) * (A.latitude - B.latitude) < 0) {
                    let pow1 = pow((P.longitude - B.longitude), 2)
                    let pow2 = pow((P.latitude - B.latitude),2)
                return sqrt(pow1 + pow2)
            }
            let numerator =
                (P.longitude * (A.latitude - B.latitude) +
                P.latitude * (B.longitude - A.longitude) +
                (A.longitude * B.latitude - B.longitude * A.latitude))
            let denominator = sqrt(
                pow(B.longitude - A.longitude, 2) +
                pow(B.latitude - A.latitude, 2))
            return abs(numerator / denominator)
    }
    
    func addUserLocation(location:CLLocationCoordinate2D) -> Step? {
        var closestStep:Step?
        if !self.trackingFinished {
            let now = NSDate()
            self.storeTrackPoint(now, location: location)
            
            var minDistance:Float = FLT_MAX
            if let steps = plan?.steps {
                let previousStep = steps[0] as! Step
                closestStep = previousStep
                for step in steps {
                    if let step = step as? Step {
                        if let previousStepCoordinate = previousStep.coordinate?.locationCoordinate2D, stepCoordinate = step.coordinate?.locationCoordinate2D {
                            let distance = Float(self.distanceFromLocation(location: location, toLineSegmentWithLocation: previousStepCoordinate, andLocation: stepCoordinate))
                            if distance < minDistance {
                                minDistance = distance
                                closestStep = step
                            }
                        }
                    }
                }
                NSLog("Distance from track: %f \n is off track: %@", minDistance, Bool(minDistance > self.offTrackDegree))
                historyTuples.append((now, minDistance))
                //update historyTuples
                let i = 0
                NSLog("\ttuplesCount %i", self.historyTuples.count)
                for tuple in historyTuples {
                    if now.timeIntervalSinceDate(tuple.time) > self.historyTreshold {
                        historyTuples.removeAtIndex(i)
                        NSLog("\ttuple deleted")
                    } else {
                        break
                    }
                }
            }
        }
        return closestStep
    }
    
    private func storeTrackPoint(now:NSDate, location:CLLocationCoordinate2D) {
        let addTrackPoint:(NSMutableOrderedSet, NSDate, CLLocationCoordinate2D) -> Void = {(mutableSet, now, location) in
            //add trackpoint
            let trackPoint = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(TrackPoint), inManagedObjectContext: DataController.sharedInstance.managedObjectContext) as! TrackPoint
            trackPoint.time = now
            trackPoint.lat = location.latitude
            trackPoint.lon = location.longitude
            mutableSet.addObject(trackPoint)
        }
        
        var mutableSet = NSMutableOrderedSet()
        if let trackPoints = self.track.trackPoints {
            if trackPoints.count == 0 {
                addTrackPoint(mutableSet, now, location)
                //add address of start / origin
                Geocoder.geocodeLocation(location, completion: { (address, error) -> Void in
                    if let error = error {
                        
                    } else {
                        self.track.origin = address
                    }
                })
            } else {
                //count distance
                if let lastTrackPoint = (trackPoints.lastObject as? TrackPoint) {
                    if let lat = lastTrackPoint.lat?.doubleValue, lon = lastTrackPoint.lon?.doubleValue {
                        let previousLocation = CLLocation(latitude: lat, longitude: lon)
                        let newLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
                        if let currentDistance = self.track.distance?.doubleValue {
                            self.track.distance = NSNumber(double: currentDistance + newLocation.distanceFromLocation(previousLocation))
                        }
                    }
                }
                
                mutableSet = NSMutableOrderedSet(orderedSet: trackPoints)
                addTrackPoint(mutableSet, now, location)
            }
        }
        self.track.trackPoints = mutableSet
    }
    
    func closeTracking() {
        if !self.trackingFinished {
            self.trackingFinished = true
            //add address of destination
            if let trackPoint = self.track.trackPoints?.lastObject as? TrackPoint {
                if let lat = trackPoint.lat?.doubleValue, lon = trackPoint.lon?.doubleValue {
                    let locationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    Geocoder.geocodeLocation(locationCoordinate, completion: { (address, error) -> Void in
                        if let error = error {
                            
                        } else {
                            self.track.destination = address
                        }
                    })
                }
            }
            //count time
            if let startDate = self.track.trackDate {
                self.track.duration = NSDate().timeIntervalSinceDate(startDate)
            }
            
            DataController.sharedInstance.saveContext()
        }
    }
    
    func gpxString() -> String {
        self.closeTracking()
        return self.track.gpxString()
    }
    
    func isUserOnTrack() -> Bool? {
        if self.plan == nil {
            return nil
        }
        var numberOffTrackTuples = 0
        for tuple in historyTuples {
            if tuple.distance > self.offTrackDegree {
                numberOffTrackTuples++
            }
        }
        var isOffTrack = false
        if historyTuples.count > 6 {
            isOffTrack = Float(numberOffTrackTuples) > (self.percentOffTrackRecords * Float(historyTuples.count))
        }
        return !isOffTrack
    }
    
    func isUserInDestination(locationCoordinate:CLLocationCoordinate2D) -> Bool? {
        if let destinationCoordinate = self.plan?.journey?.destination?.locationCoordinate2D {
            let location = CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
            let destinationLocation = CLLocation(latitude: destinationCoordinate.latitude, longitude: destinationCoordinate.longitude)
            let distance = destinationLocation.distanceFromLocation(location)
            return distance < 50
        }
        return nil
    }
}