//
//  TrackTableViewCell.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 21/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit
import Mapbox

class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var mapWrapperView: UIView!
    
    var mapView:MGLMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let mapView:MGLMapView
        mapView = MGLMapView(frame: self.mapWrapperView.bounds)
        mapView.userInteractionEnabled = false
        mapView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.mapWrapperView.addSubview(mapView)
        self.mapView = mapView
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
