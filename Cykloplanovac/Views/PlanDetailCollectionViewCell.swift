//
//  PlanDetailCollectionViewCell.swift
//  Cykloplanovac
//
//  Created by Jenda on 03/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit

class PlanDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var topLeftLabel: UILabel!
    @IBOutlet weak var bottomLeftLabel: UILabel!
    @IBOutlet weak var topRightLabel: UILabel!
    @IBOutlet weak var bottomRightLabel: UILabel!
    
    
}
