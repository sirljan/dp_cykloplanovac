//
//  Plan.swift
//  Cykloplanovac
//
//  Created by Jenda on 16/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(Plan)
class Plan: NSManagedObject {

    let moc = DataController.sharedInstance.managedObjectContext
    
    func setup(withDictionary dictionary:NSDictionary) {
        planId = dictionary.objectForKey("planId") as? NSNumber
        if let boundingBoxDict = dictionary.objectForKey("boundingBox") as? NSDictionary {
            let boundingBox = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(BoundingBox), inManagedObjectContext: moc) as? BoundingBox
            boundingBox?.setup(withDictionary: boundingBoxDict)
            self.boundingBox = boundingBox
        }
        else
        {
            self.boundingBox = nil
        }
        if let criteriaDict = dictionary.objectForKey("criteria") as? NSDictionary {
            let criteria = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(Criteria), inManagedObjectContext: moc) as? Criteria
            criteria?.setup(withDictionary: criteriaDict)
            self.criteria = criteria
        }
        else
        {
            self.criteria = nil
        }
        planDescription = dictionary.objectForKey("description") as? String
        elevationGain = dictionary.objectForKey("elevationGain") as? NSNumber
        elevationDrop = dictionary.objectForKey("elevationDrop") as? NSNumber
        distance = dictionary.objectForKey("length") as? NSNumber
        duration = dictionary.objectForKey("duration") as? NSTimeInterval
        consumedEnergy = dictionary.objectForKey("consumedEnergy") as? NSNumber
        
        let stepsMutable = NSMutableOrderedSet()
        if let stepsArray = dictionary["steps"] as? [NSDictionary] {
            for stepDict in stepsArray {
                if let step = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(Step), inManagedObjectContext: moc) as? Step {
                    step.setup(stepDict)
                    stepsMutable.addObject(step)
                }
            }
        }
        self.steps = stepsMutable
    }
}
