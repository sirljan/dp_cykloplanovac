//
//  DegE6.swift
//  Cykloplanovac
//
//  Created by Jenda on 16/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(DegE6)
class DegE6: NSManagedObject {
    var degree : Double? {
        get {
            if let deg = self.degreeE6?.doubleValue {
                return deg / 1e6
            }
            return nil
        }
        set {
            if let deg = newValue {
                self.degreeE6 = deg * 1e6
            }
        }
    }
}
