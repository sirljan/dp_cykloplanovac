//
//  Journey+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Journey {

    @NSManaged var avarageSpeedKmPH: NSNumber?
    @NSManaged var city: String?
    @NSManaged var creationTimeStamp: String?
    @NSManaged var name: String?
    @NSManaged var region: String?
    @NSManaged var responseId: NSNumber?
    @NSManaged var status: String?
    @NSManaged var destination: NavCoordinate?
    @NSManaged var origin: NavCoordinate?
    @NSManaged var plans: NSOrderedSet?
    @NSManaged var user: User?
    @NSManaged var track: NSManagedObject?

}
