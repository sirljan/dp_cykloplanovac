//
//  TrackPoint.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(TrackPoint)
class TrackPoint: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        
        if var latitude = self.lat?.doubleValue {
            latitude = latitude * 1e6
            dictionary.setValue(latitude, forKey: key_latE6)
        }
        if var lon = self.lon?.doubleValue {
            lon = lon * 1e6
            dictionary.setValue(lon, forKey: key_lonE6)
        }
        if let timestamp = self.time {
            dictionary.setObject(timestamp, forKey: "timestamp")
        }
        return dictionary
    }
}
