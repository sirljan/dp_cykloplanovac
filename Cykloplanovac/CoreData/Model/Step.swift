//
//  Step.swift
//  Cykloplanovac
//
//  Created by Jenda on 16/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

enum Surface : String {
    case unpaved = "unpaved"
    case pavedSmooth = "pavedSmooth"
    case pavedCobbleStone = "pavedCobbleStone"
    case unknown
}

enum RoadType : String {
    case primary =      "PRIMARY"
    case secondary =    "SECONDARY"
    case tertiary =     "TERTIARY"
    case footway =      "FOOTWAY"
    case cycleway =     "CYCLEWAY"
    case steps =        "STEPS"
    case road =         "ROAD"
    case unknow
}

@objc(Step)
class Step: NSManagedObject {

    var surface:Surface? {
        get {
            return Surface(rawValue: self.surfaceRaw!)
        }
        set {
            self.surfaceRaw = newValue?.rawValue
        }
    }
    
    func setup(dictionary:NSDictionary)
    {
        streetName = dictionary["streetName"] as? String
        surface = dictionary["surface"] as? Surface
        roadTypeRaw = dictionary["roadType"] as? String
        bicycleRouteNumber = dictionary["bicycleRouteNumber"] as? String
        travelTimeToNextStep = dictionary["travelTimeToNextStep"] as? NSTimeInterval
        distanceToNextStep = dictionary["distanceToNextStep"] as? NSNumber
        angle = dictionary["angle"] as? NSNumber
        
        let moc = DataController.sharedInstance.managedObjectContext
        let coordinate = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(NavCoordinate), inManagedObjectContext: moc) as! NavCoordinate
        coordinate.setup(dictionary["coordinate"] as? NSDictionary)
        
        self.coordinate = coordinate
    }

}
