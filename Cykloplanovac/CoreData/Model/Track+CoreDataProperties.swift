//
//  Track+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 27/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Track {

    @NSManaged var destination: String?
    @NSManaged var distance: NSNumber?
    @NSManaged var duration: NSNumber?
    @NSManaged var origin: String?
    @NSManaged var trackDate: NSDate?
    @NSManaged var rating: NSNumber?
    @NSManaged var textualFeedback: String?
    @NSManaged var journey: Journey?
    @NSManaged var plan: Plan?
    @NSManaged var trackPoints: NSOrderedSet?
    @NSManaged var user: User?

}
