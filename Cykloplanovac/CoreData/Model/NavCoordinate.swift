//
//  NavCoordinate.swift
//  Cykloplanovac
//
//  Created by Jenda on 16/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

enum NavCoordinateType : String {
    case origin = "ORIGIN"
    case destination = "DESTINATION"
    case waypoint = "WAYPOINT"
    case unknown
}

let key_lonE6 = "lonE6"
let key_latE6 = "latE6"
let key_elevation = "elevation"
let key_coordinateType = "type"

@objc(NavCoordinate)
class NavCoordinate: NSManagedObject {

    let moc = DataController.sharedInstance.managedObjectContext
    
    var locationCoordinate2D:CLLocationCoordinate2D? {
        get {
            if let latDegree = self.latE6?.degree {
                if let lonDegree = self.lonE6?.degree {
                    let locationCoordinate = CLLocationCoordinate2DMake(latDegree, lonDegree)
                    return locationCoordinate
                }
            }
            return nil
        }
    }
    var coordinateType: NavCoordinateType? {
        get {
            return NavCoordinateType(rawValue: self.coordinateTypeRaw!)
        }
        set {
            self.coordinateTypeRaw = newValue?.rawValue
        }
    }
    
    func setup(longitudeE6 lon:DegE6, latitudeE6 lat:DegE6, elevation:Double?, type:NavCoordinateType?) {
        self.lonE6 = lon
        self.latE6 = lat
        self.elevation = elevation
        self.coordinateType = type
    }
    
    func setup(locationCoordinate2D:CLLocationCoordinate2D, type:NavCoordinateType?) {
        let lonE6 = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6
        lonE6.degree = locationCoordinate2D.longitude
        self.lonE6 = lonE6
        let latE6 = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6
        latE6.degree = locationCoordinate2D.latitude
        self.latE6 = latE6
        self.elevation = nil
        self.coordinateType = type
    }
    
    func setup(dictionary: NSDictionary?) {
        let lonE6 = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6
        lonE6.degreeE6 = dictionary?[key_lonE6] as? Double
        self.lonE6 = lonE6
        let latE6 = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6
        latE6.degreeE6 = dictionary?[key_latE6] as? Double
        self.latE6 = latE6
        self.elevation = dictionary?[key_elevation] as? Double
        self.coordinateType = dictionary?[key_coordinateType] as? NavCoordinateType
    }
    
    func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if let lon = self.lonE6?.degreeE6 {
            dictionary.setObject(lon, forKey: key_lonE6)
        }
        if let lat = self.latE6?.degreeE6 {
            dictionary.setObject(lat, forKey: key_latE6)
        }
        if let elev = self.elevation {
            dictionary.setObject(elev, forKey: key_elevation)
        }
        if let type = self.coordinateType {
            dictionary.setObject(type.rawValue, forKey: key_coordinateType)
        }
        return dictionary
    }

}
