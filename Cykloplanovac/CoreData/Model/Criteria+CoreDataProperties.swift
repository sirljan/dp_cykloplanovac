//
//  Criteria+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jenda on 04/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Criteria {

    @NSManaged var physicalEffort: NSNumber?
    @NSManaged var stress: NSNumber?
    @NSManaged var travelTime: NSNumber?
    @NSManaged var plan: Plan?

}
