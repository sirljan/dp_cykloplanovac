//
//  Journey.swift
//  Cykloplanovac
//
//  Created by Jenda on 15/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(Journey)
class Journey: NSManagedObject {
    var client:String?
    
    func setupJourney(origin:NavCoordinate, destination:NavCoordinate, client:String) {
        self.origin = origin
        self.destination = destination
        self.client = client
    }
    
    func mergeWithDictionary(JSONDictionary:NSDictionary?) {
        if let JSONDictionary = JSONDictionary {
            // Loop
            for (key, value) in JSONDictionary {
                if let keyNameString = key as? String {
                    // If property exists
                    if keyNameString == "plans" {
                        if let plansDictionary = value as? [NSDictionary] {
                            print("recieved \(plansDictionary.count) plan(s)")
                            for planDictionary in plansDictionary {
                                let moc = DataController.sharedInstance.managedObjectContext
                                let plan = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(Plan), inManagedObjectContext: moc) as! Plan
                                plan.setup(withDictionary: planDictionary)
                                self.mutableOrderedSetValueForKey("plans").addObject(plan)
                            }
                        }
                    } else if (self.respondsToSelector(NSSelectorFromString(keyNameString))) {
                        self.setValue(value, forKey: keyNameString)
                    }
                }
            }
            // Or you can do it with using
            // self.setValuesForKeysWithDictionary(JSONDictionary)
            // instead of loop method above
        }
        if self.status == "OUT_OF_BOUNDS" {
            print(self.status!)
        }
    }
    
    func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        if let client = self.client {
            dictionary.setObject(client, forKey: "client")
        }
        if let originDict = self.origin?.dictionaryRepresentation(), let destinationDict = self.destination?.dictionaryRepresentation() {
            dictionary.setObject(originDict, forKey: "origin")
            dictionary.setObject(destinationDict, forKey: "destination")
        }
        if let city = self.city {
            dictionary.setObject(city, forKey: "city")
        }
        if let speed = self.user?.averageSpeedKmPH {
            dictionary.setObject(speed, forKey: "averageSpeedKmPH")
        }
        return dictionary
    }
    
    func planWithPlanId(planId:NSNumber) -> Plan? {
        if let plans = self.plans {
            for plan in plans {
                if let plan = plan as? Plan {
                    if plan.planId == planId {
                        return plan
                    }
                }
            }
        }
        return nil
    }
}
