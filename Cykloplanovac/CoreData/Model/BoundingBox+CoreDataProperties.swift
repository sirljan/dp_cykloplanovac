//
//  BoundingBox+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jenda on 17/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BoundingBox {

    @NSManaged var bottomRightLat: DegE6?
    @NSManaged var bottomRightLon: DegE6?
    @NSManaged var plan: Plan?
    @NSManaged var topLeftLat: DegE6?
    @NSManaged var topLeftLon: DegE6?

}
