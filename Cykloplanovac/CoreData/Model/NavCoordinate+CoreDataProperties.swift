//
//  NavCoordinate+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jenda on 14/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension NavCoordinate {

    @NSManaged var address: String?
    @NSManaged var coordinateTypeRaw: String?
    @NSManaged var elevation: NSNumber?
    @NSManaged var journeyDestination: Journey?
    @NSManaged var journeyOrigin: Journey?
    
    @NSManaged var latE6: DegE6?
    @NSManaged var lonE6: DegE6?
    
    @NSManaged var step: Step?
    @NSManaged var userHome: User?
    @NSManaged var userWork: User?

}
