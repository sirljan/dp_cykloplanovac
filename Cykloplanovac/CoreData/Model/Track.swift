//
//  Track.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(Track)
class Track: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func gpxString() -> String {
        let gpxRoot = GPXRoot(creator: client)
        let gpxTrack = GPXTrack()
        gpxTrack.name = ""
        if let address = self.journey?.destination?.address {
            gpxTrack.name = "\(address)"
        }
        gpxTrack.name = "\(gpxTrack.name) \(NSDate())"
        
        let gpxTrackSegment = GPXTrackSegment()
        gpxTrack.addTracksegment(gpxTrackSegment)
        
        if let trackPoints = self.trackPoints {
            for trackPoint in trackPoints {
                if let trackPoint = trackPoint as? TrackPoint {
                    if let lat = trackPoint.lat?.floatValue, lon = trackPoint.lon?.floatValue {
                        let gpxTrackPoint = GPXTrackPoint.trackpointWithLatitude(CGFloat(lat), longitude: CGFloat(lon))
                        gpxTrackSegment.addTrackpoint(gpxTrackPoint)
                    }
                }
            }
        }
        gpxRoot.addTrack(gpxTrack)
        let gpxString = gpxRoot.gpx()
        print(gpxString)
        return gpxString
    }
    
    func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setObject(client, forKey: "client")
        if let responseId = self.journey?.responseId?.stringValue {
            dictionary.setObject(responseId, forKey: "responseID")
        }
        if let textualFeedback = self.textualFeedback {
            dictionary.setObject(textualFeedback, forKey: "textualFeedback")
        }
        if let rating = self.rating {
            dictionary.setObject(rating, forKey: "rating")
        }
        
        if let trackPoints = self.trackPoints {
            var trackPointArray = [NSDictionary]()
//            for trackPoint in trackPoints {
//                if let trackPoint = trackPoint as? TrackPoint {
//                    trackPointArray.append(trackPoint.dictionaryRepresentation())
//                }
//            }
            dictionary.setObject(trackPointArray, forKey: "trackedJourney")
        }
        
        return dictionary
    }
}
