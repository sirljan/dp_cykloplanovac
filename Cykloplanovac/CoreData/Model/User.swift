//
//  User.swift
//  Cykloplanovac
//
//  Created by Jenda on 14/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
class User: NSManagedObject {

    var isSignedIn:Bool = false
    
    func login(login: String, password: String, completion: (success: Bool) -> Void) {
        print("communicating...")
        completion(success: false)
    }
}
