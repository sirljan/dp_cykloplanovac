//
//  User+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var averageSpeedKmPH: NSNumber?
    @NSManaged var distanceTotal: NSNumber?
    @NSManaged var name: String?
    @NSManaged var home: NavCoordinate?
    @NSManaged var journeys: NSOrderedSet?
    @NSManaged var work: NavCoordinate?
    @NSManaged var tracks: NSOrderedSet?

}
