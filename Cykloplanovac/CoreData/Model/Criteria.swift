//
//  Criteria.swift
//  Cykloplanovac
//
//  Created by Jenda on 04/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData

@objc(Criteria)
class Criteria: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    func setup(withDictionary dictionary:NSDictionary) {
        self.physicalEffort = dictionary.objectForKey("physicalEffort") as? NSNumber
        self.stress = dictionary.objectForKey("stress") as? NSNumber
        self.travelTime = dictionary.objectForKey("travelTime") as? NSNumber
    }

}
