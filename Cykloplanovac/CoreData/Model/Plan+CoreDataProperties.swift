//
//  Plan+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Plan {

    @NSManaged var consumedEnergy: NSNumber?
    @NSManaged var distance: NSNumber?
    @NSManaged var duration: NSNumber?
    @NSManaged var elevationDrop: NSNumber?
    @NSManaged var elevationGain: NSNumber?
    @NSManaged var planDescription: String?
    @NSManaged var planId: NSNumber?
    @NSManaged var boundingBox: BoundingBox?
    @NSManaged var criteria: Criteria?
    @NSManaged var journey: Journey?
    @NSManaged var steps: NSOrderedSet?
    @NSManaged var track: NSManagedObject?

}
