//
//  Step+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jenda on 17/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Step {

    @NSManaged var angle: NSNumber?
    @NSManaged var bicycleRouteNumber: String?
    @NSManaged var distanceToNextStep: NSNumber?
    @NSManaged var roadTypeRaw: String?
    @NSManaged var streetName: String?
    @NSManaged var surfaceRaw: String?
    @NSManaged var travelTimeToNextStep: NSNumber?
    @NSManaged var coordinate: NavCoordinate?
    @NSManaged var plan: Plan?

}
