//
//  TrackPoint+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 19/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension TrackPoint {

    @NSManaged var time: NSDate?
    @NSManaged var lon: NSNumber?
    @NSManaged var lat: NSNumber?
    @NSManaged var track: NSManagedObject?

}
