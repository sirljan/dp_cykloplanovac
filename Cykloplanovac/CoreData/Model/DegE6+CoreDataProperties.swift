//
//  DegE6+CoreDataProperties.swift
//  Cykloplanovac
//
//  Created by Jenda on 17/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DegE6 {

    @NSManaged var degreeE6: NSNumber?
    @NSManaged var coordinateLat: NavCoordinate?
    @NSManaged var coordinateLon: NavCoordinate?
    @NSManaged var boundingBoxBottomRightLon: BoundingBox?
    @NSManaged var boundingBoxBottomRightLat: BoundingBox?
    @NSManaged var boundingBoxTopLeftLon: BoundingBox?
    @NSManaged var boundingBoxTopLeftLat: BoundingBox?

}
