//
//  BoundingBox.swift
//  Cykloplanovac
//
//  Created by Jenda on 16/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

@objc(BoundingBox)
class BoundingBox: NSManagedObject {
    
    var topLeftCoordinate:CLLocationCoordinate2D? {
        get {
            if let lat = self.topLeftLat?.degree, lon = self.topLeftLon?.degree {
                return CLLocationCoordinate2DMake(lat, lon)
            }
            return nil
        }
        set {
            self.topLeftLat?.degree = newValue?.latitude
            self.topLeftLon?.degree = newValue?.longitude
        }
    }
    
    var bottomRightCoordinate:CLLocationCoordinate2D? {
        get {
            if let lat = self.bottomRightLat?.degree, lon = self.bottomRightLon?.degree {
                return CLLocationCoordinate2DMake(lat, lon)
            }
            return nil
        }
        set {
            self.bottomRightLat?.degree = newValue?.latitude
            self.bottomRightLon?.degree = newValue?.longitude
        }
    }
    
    func setup(withDictionary dictionary:NSDictionary) {
        let moc = DataController.sharedInstance.managedObjectContext
        self.topLeftLat = (NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6)
        self.topLeftLat!.degreeE6 = NSNumber(double: dictionary.valueForKeyPath("topLeft.latE6") as! Double)
        
        self.topLeftLon = (NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6)
        self.topLeftLon!.degreeE6 = NSNumber(double: dictionary.valueForKeyPath("topLeft.lonE6") as! Double)
        
        self.bottomRightLat = (NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6)
        self.bottomRightLat!.degreeE6 = NSNumber(double: dictionary.valueForKeyPath("bottomRight.latE6") as! Double)
        
        self.bottomRightLon = (NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(DegE6), inManagedObjectContext: moc) as! DegE6)
        self.bottomRightLon!.degreeE6 = NSNumber(double: dictionary.valueForKeyPath("bottomRight.lonE6") as! Double)
    }
}
