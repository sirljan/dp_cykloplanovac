//
//  JourneyViewController.swift
//  Cykloplanovac
//
//  Created by Jenda on 15/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit

class JourneyViewController: UIViewController {
    
    var journey:Journey?

    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let journey = self.journey {
            self.label.text = "\(journey.responseId)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
