//
//  PlanViewController.swift
//  Cykloplanovac
//
//  Created by Jenda on 28/10/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import ContactsUI

enum ContactPickerSelection {
    case Home
    case Work
}

@available(iOS 9.0, *)
protocol FavouritesViewControllerDelegate {
    func favouritesViewController(sender:FavouritesViewController, didSelectAddress address:String)
}

@available(iOS 9.0, *)
class PlanViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSFetchedResultsControllerDelegate, CNContactPickerDelegate, FavouritesViewControllerDelegate {
    var delegate:PlanViewControllerDelegate?
    private var fetchedResultsController: NSFetchedResultsController!
    private let currentLocationString = NSLocalizedString("Current Location", comment: "text")
    private var contactSelection:ContactPickerSelection?
    
    @IBOutlet private weak var originTextField: UITextField!
    @IBOutlet private weak var destinationTextField: UITextField!
    private weak var lastEditingTextField: UITextField?
    
    @IBOutlet private var planBarButton: UIBarButtonItem!
    
    @IBOutlet weak var customTableView: UITableView!
    var tableView:UITableView {
        get {
            return self.customTableView
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setting left views
        let originLabel = UILabel()
        originLabel.text = NSLocalizedString("Start:", comment: "textfield inner tittle")
        originLabel.translatesAutoresizingMaskIntoConstraints = false
        originTextField.leftView = originLabel
        originTextField.leftViewMode = UITextFieldViewMode.Always
        let destinationLabel = UILabel()
        destinationLabel.text = NSLocalizedString("End:", comment: "textfield inner tittle")
        destinationLabel.sizeToFit()
        destinationTextField.leftView = destinationLabel
        destinationTextField.leftViewMode = UITextFieldViewMode.Always

        if let startAddress = self.delegate?.planViewControllerStartAddress() {
            self.originTextField.text = startAddress
        } else {
            originTextField.text = currentLocationString
            self.dehighlightCurrentLocationInTextField(originTextField)
        }
        
        if let endAddress = self.delegate?.planViewControllerEndAddress() {
            self.destinationTextField.text = endAddress
        }
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.barTintColor = UIColor.whiteColor()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "resignKeyboard")
        
        toolbar.items = [flexibleSpace, doneButton]
        self.originTextField.inputAccessoryView = toolbar
        self.destinationTextField.inputAccessoryView = toolbar

        self.textFieldEditingChanged(nil)
        self.initializeFetchedResultsController()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func resignKeyboard() {
        self.view.endEditing(false)
    }
    
    func displayError(erroTitle: String, errorMessage: String) {
        let alertController = UIAlertController(title: erroTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title:NSLocalizedString("OK", comment: "alert button title"), style: UIAlertActionStyle.Cancel) { (alertAction:UIAlertAction) -> Void in

        }
        alertController.addAction(action)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FavouritesViewController" {
            let navigationController = segue.destinationViewController as? UINavigationController
            let favouritesViewController = navigationController?.topViewController as? FavouritesViewController
            favouritesViewController?.delegate = self
            self.lastEditingTextField = self.originTextField.isFirstResponder() ? self.originTextField : nil
            self.lastEditingTextField = self.destinationTextField.isFirstResponder() ? self.destinationTextField : nil
            if self.lastEditingTextField == nil {
                if let text = self.originTextField.text where text.isEmpty {
                    lastEditingTextField = self.originTextField
                } else if let text = self.destinationTextField.text where text.isEmpty {
                    lastEditingTextField = self.destinationTextField
                }
            }
        }
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    @IBAction func switchTapped(sender: AnyObject) {
        let string = self.originTextField.text
        self.originTextField.text = self.destinationTextField.text
        self.destinationTextField.text = string
        self.dehighlightCurrentLocationInTextField(originTextField)
        self.dehighlightCurrentLocationInTextField(destinationTextField)
        self.defaultColorsForTextField(originTextField, string: originTextField.text!)
        self.defaultColorsForTextField(destinationTextField, string: destinationTextField.text!)
    }
    
    @IBAction func currentLocationTapped(sender: AnyObject) {
        if self.lastEditingTextField == nil {
            if let text = self.originTextField.text where text.isEmpty {
                lastEditingTextField = self.originTextField
            } else if let text = self.destinationTextField.text where text.isEmpty {
                lastEditingTextField = self.destinationTextField
            }
        }
        if let lastEditingTextField = self.lastEditingTextField {
            lastEditingTextField.text = currentLocationString
            self.dehighlightCurrentLocationInTextField(lastEditingTextField)
        }
    }
    
    @IBAction func homeTapped(sender: AnyObject) {
        if self.lastEditingTextField == nil {
            if let text = self.originTextField.text where text.isEmpty {
                lastEditingTextField = self.originTextField
            } else if let text = self.destinationTextField.text where text.isEmpty {
                lastEditingTextField = self.destinationTextField
            }
        }
        if let address = self.delegate?.planViewControllerUser().home?.address {
            self.lastEditingTextField?.text = address
        } else {
            self.contactSelection = ContactPickerSelection.Home
            self.presentContactPickerViewController()
        }
        self.planPressed(nil)
    }
    
    @IBAction func workTapped(sender: AnyObject) {
        if self.lastEditingTextField == nil {
            if let text = self.originTextField.text where text.isEmpty {
                lastEditingTextField = self.originTextField
            } else if let text = self.destinationTextField.text where text.isEmpty {
                lastEditingTextField = self.destinationTextField
            }
        }
        if let address = self.delegate?.planViewControllerUser().work?.address {
            self.lastEditingTextField?.text = address
        } else {
            self.contactSelection = ContactPickerSelection.Work
            self.presentContactPickerViewController()
        }
        self.planPressed(nil)
    }
    
    func presentContactPickerViewController() {
        let contactPickerViewController = CNContactPickerViewController()
        contactPickerViewController.delegate = self
        contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "postalAddresses.@count > 0")
        contactPickerViewController.predicateForSelectionOfContact = NSPredicate(format: "postalAddresses.@count == 1")
        contactPickerViewController.displayedPropertyKeys = [CNContactPostalAddressesKey]
        self.presentViewController(contactPickerViewController, animated: true, completion: nil)
    }
    
    func geocodeAddressString(address:String, completion:(coordinate: CLLocationCoordinate2D?, error:NSError?) -> Void) {
        if address == self.currentLocationString {
            completion(coordinate: self.delegate?.planViewControllerUserLocation(), error: nil)
        } else {
            Geocoder.geocodeAddressString(address, userCoordinate: self.delegate?.planViewControllerUserLocation(), completion: { (coordinate, error) -> Void in
                if let error = error {
                    let errorTitle = NSLocalizedString("Geocode failed", comment: "error title")
                    self.displayError(errorTitle, errorMessage: error.localizedDescription)
                } else {
                    print(coordinate)
                    completion(coordinate: coordinate, error: nil)
                }
            })
        }
    }
    
    @IBAction func planPressed(sender: AnyObject?) {
        self.view.endEditing(false)
        
        let activityView = UIActivityIndicatorView(frame: CGRectMake(0,0,20,20))
        activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        let activityButton = UIBarButtonItem(customView: activityView)
        self.navigationItem.rightBarButtonItem = activityButton
        activityView.startAnimating()
        self.geocodeAddresses()
    }
    
    func geocodeAddresses() {
        if let originString = originTextField.text, let destinationString = self.destinationTextField.text where !originString.isEmpty && !destinationString.isEmpty{
            self.geocodeAddressString(originString, completion: { (originCoordinate, error) -> Void in
                if error == nil {
                    self.geocodeAddressString(destinationString, completion: { (destinationCoordinate, error) -> Void in
                        if error == nil {
                            self.navigationItem.rightBarButtonItem = self.planBarButton
                            //delay shouldn't be need but there is probably some bug
                            delay(0.01, closure: { () -> () in
                                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                    if let originCoordinate = originCoordinate, destinationCoordinate = destinationCoordinate {
                                        self.delegate?.planViewControllerPlanJourney(fromOrigin: originCoordinate, toDestination: destinationCoordinate, originAddress: originString, destinationAddress: destinationString)
                                    }
                                })
                            })
                        } else {
                            self.navigationItem.rightBarButtonItem = self.planBarButton
                        }
                    })
                } else {
                    self.navigationItem.rightBarButtonItem = self.planBarButton
                }
            })
        } else {
            self.navigationItem.rightBarButtonItem = self.planBarButton
        }
    }
    
    func initializeFetchedResultsController() {
        let request = NSFetchRequest(entityName: NSStringFromClass(Journey))
        let jorneySort = NSSortDescriptor(key: "name", ascending: true)
        let dataSort = NSSortDescriptor(key: "creationTimeStamp", ascending: true)
        request.sortDescriptors = [jorneySort, dataSort]
        
        let moc = DataController.sharedInstance.managedObjectContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func didSelectAddress(address: String) {
        self.lastEditingTextField?.text = address
        self.textFieldEditingChanged(self.lastEditingTextField)
        self.planPressed(nil)
    }

    // MARK: FavouritesViewControllerDelegate
    
    func favouritesViewController(sender: FavouritesViewController, didSelectAddress address: String) {
        self.didSelectAddress(address)
    }
    
    // MARK: textFields
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.planPressed(nil)
    }
    
    @IBAction func textFieldEditingChanged(sender: UITextField?) {
        if let originIsEmpty = self.originTextField.text?.isEmpty, let destinationIsEmpty = self.destinationTextField.text?.isEmpty {
            self.planBarButton.enabled = !originIsEmpty && !destinationIsEmpty
        }
        if let textField = sender {
            self.highlightCurrentLocationInTextField(textField)
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.lastEditingTextField = textField
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.highlightCurrentLocationInTextField(textField)
        
        //set return key
        let textFields = [self.originTextField, self.destinationTextField]
        var emptyTextFields = [UITextField]()
        for t in textFields where t.text!.isEmpty {
            emptyTextFields.append(t)
        }
        let next = emptyTextFields.count > 1 || (emptyTextFields.count == 1 && !emptyTextFields.first!.isEqual(textField))
        textField.returnKeyType = next ? UIReturnKeyType.Next : UIReturnKeyType.Done
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        self.dehighlightCurrentLocationInTextField(textField)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.Next {
            if textField.isEqual(self.originTextField) {
                self.destinationTextField.becomeFirstResponder()
            } else if textField.isEqual(self.destinationTextField) {
                self.originTextField.becomeFirstResponder()
            }
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text == self.currentLocationString {
            textField.text = self.currentLocationString+" "
            self.defaultColorsForTextField(textField, string: string)
        }
        return true
    }
    
    func highlightCurrentLocationInTextField(textField:UITextField) {
        //highlight current location
        if textField.text == self.currentLocationString {
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(string: currentLocationString)
            attributedString.addAttribute(NSBackgroundColorAttributeName, value:self.view.tintColor, range: NSMakeRange(0, currentLocationString.characters.count))
            textField.attributedText = attributedString
            textField.textColor = UIColor.whiteColor()
        }
    }
    
    func dehighlightCurrentLocationInTextField(textField:UITextField) {
        //dehighlight current location
        if textField.text == self.currentLocationString {
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(string: currentLocationString)
            attributedString.addAttribute(NSBackgroundColorAttributeName, value:UIColor.clearColor(), range: NSMakeRange(0, currentLocationString.characters.count))
            textField.attributedText = attributedString
            textField.textColor = self.view.tintColor
        }
    }
    
    func defaultColorsForTextField(textField:UITextField, string:String) {
        if textField.text != self.currentLocationString {
            let tempString = " "
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(string: tempString)
            attributedString.addAttribute(NSBackgroundColorAttributeName, value:UIColor.clearColor(), range: NSMakeRange(0, tempString.characters.count))
            textField.attributedText = attributedString
            textField.textColor = UIColor.blackColor()
            textField.text = string
        }
    }
    
    // MARK: TableView Data Source
    
    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let journey = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Journey
        // Populate cell from the NSManagedObject instance
        print("Object for configuration: \(journey.responseId)")
        if let originAddress = journey.origin?.address, plans = journey.plans where plans.count > 0, let distance = (plans.objectAtIndex(0) as? Plan)?.distance {
            cell.textLabel?.text = "\(originAddress)\t~\(round(distance.doubleValue/1e3))km"
        }
        if let destinationAddress = journey.destination?.address {
            cell.detailTextLabel?.text = "\(NSLocalizedString("from", comment: "origin of journey")) \(destinationAddress)"
        }        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("journeyCell", forIndexPath: indexPath)
        // Set up the cell
        self.configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections!.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    //problem with cascade deleting
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if editingStyle == UITableViewCellEditingStyle.Delete {
//            //delete row
//            print("delete editingStyle", indexPath)
//            
//            let journey = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Journey
//            DataController.sharedInstance.managedObjectContext.deleteObject(journey)
//        }
//    }
    
    // MARK: TableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // WARNING: todo didSelect
        let journey = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Journey
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.delegate?.planViewControllerPlanJourney(journey)
        })
    }
    
    //MARK: CNContactPickerDelegate
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContactProperty contactProperty: CNContactProperty) {
        if let postalAddress = contactProperty.value as? CNPostalAddress {
            self.didSelectPostalAdress(postalAddress)
        }
        else
        {
            NSLog("ERROR: No postalAddress")
        }
    }
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        if let postalAddress = contact.postalAddresses[0].value as? CNPostalAddress {
            self.didSelectPostalAdress(postalAddress)
        }
        else
        {
            NSLog("ERROR: No postalAddress")
        }
    }
    
    func didSelectPostalAdress(postalAddress:CNPostalAddress) {
        let address = "\(postalAddress.street), \(postalAddress.city), \(postalAddress.country), \(postalAddress.postalCode)"
        if let contactSelection = self.contactSelection {
            switch contactSelection {
            case ContactPickerSelection.Home:
                self.delegate?.planViewControllerUser().home?.address = address
            case ContactPickerSelection.Work:
                self.delegate?.planViewControllerUser().work?.address = address
            }
        }
        self.didSelectAddress(address)
    }
    
    func contactPickerDidCancel(picker: CNContactPickerViewController) {
        
    }
    
    //MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Move:
            break
        case .Update:
            break
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(self.tableView.cellForRowAtIndexPath(indexPath!)!, indexPath: indexPath!)
        case .Move:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            self.tableView.insertRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
}
