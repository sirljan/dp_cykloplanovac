//
//  TracksViewController.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 21/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

let string_error = NSLocalizedString("Error", comment: "alert title")
let string_success = NSLocalizedString("Success", comment: "alert title")

import CoreData
import Mapbox

class TracksViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    private var fetchedResultsController:NSFetchedResultsController!
    var isObserved = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeFetchedResultsController()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let label = UILabel()
        label.text = NSLocalizedString("Here you can send your tracks to NKDP", comment: "text, advice")
        self.tableView.tableHeaderView = label
    }
    
    func initializeFetchedResultsController() {
        let request = NSFetchRequest(entityName: NSStringFromClass(Track))
        let dateSort = NSSortDescriptor(key: "trackDate", ascending: true)
        request.sortDescriptors = [dateSort]
        
        let moc = DataController.sharedInstance.managedObjectContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func extractCode(notification: NSNotification) -> String? {
        let url: NSURL? = (notification.userInfo as!
            [String: AnyObject])[UIApplicationLaunchOptionsURLKey] as? NSURL
        
        // [1] extract the code from the URL
        return self.parametersFromQueryString(url?.query)["code"]
    }
    
    func parametersFromQueryString(queryString: String?) -> [String: String] {
        var parameters = [String: String]()
        if (queryString != nil) {
            let parameterScanner: NSScanner = NSScanner(string: queryString!)
            var name:NSString? = nil
            var value:NSString? = nil
            while (parameterScanner.atEnd != true) {
                name = nil;
                parameterScanner.scanUpToString("=", intoString: &name)
                parameterScanner.scanString("=", intoString:nil)
                value = nil
                parameterScanner.scanUpToString("&", intoString:&value)
                parameterScanner.scanString("&", intoString:nil)
                if (name != nil && value != nil) {
                    parameters[name!.stringByRemovingPercentEncoding!] = value?.stringByRemovingPercentEncoding!
                }
            }
        }
        return parameters
    }
    
    func sendTrack(track:Track) {
        // 1 Replace with client id /secret
        let clientID = ""           //"849808077655-jtc8ng62enokngcaj87mp4hhdfempi5m.apps.googleusercontent.com"
        let clientSecret = ""
        
        //test
        let subDomain = "test2015"
        //production 
//        let subDomain = "dpnk2015"
        
        let baseURLString = "http://\(subDomain).dopracenakole.net"
        let tokenPath = "/oauth2/token"
        let scope = "read".urlEncode()
        let redirect_uri = "com.cvut.fel.iOS.Cykloplanovac:/oauth2Callback"
        
        let authorizeURLString = "\(baseURLString)/oauth2/authorize"
        let gpxUploadURLString = "\(baseURLString)/rest/gpx"
        
        if !isObserved {
            // 2 Add observer
            NSNotificationCenter.defaultCenter().addObserverForName(
                "AGAppLaunchedWithURLNotification",
                object: nil,
                queue: nil,
                usingBlock: { (notification: NSNotification!) -> Void in
                    // [5] extract code
                    let code = self.extractCode(notification)
                    
                    // [6] carry on oauth2 code auth grant flow with AFOAuth2Manager
                    let manager = AFOAuth2Manager(baseURL: NSURL(string: baseURLString),
                        clientID: clientID,
                        secret: clientSecret)
                    manager.useHTTPBasicAuthentication = false
                    
                    // [7] exchange authorization code for access token
                    manager.authenticateUsingOAuthWithURLString(tokenPath,
                        code: code,
                        redirectURI: redirect_uri,
                        success: { (cred: AFOAuthCredential!) -> Void in
                            
                            // [8] Set credential in header
                            manager.requestSerializer.setValue("Bearer \(cred.accessToken)",
                                forHTTPHeaderField: "Authorization")
                            
                            // [9] upload
                            manager.POST(gpxUploadURLString,
                                parameters: nil,
                                constructingBodyWithBlock: { (form: AFMultipartFormData!) -> Void in
                                    if let gpxData = track.gpxString().dataUsingEncoding(NSUTF8StringEncoding) {
                                        form.appendPartWithFileData(gpxData,
                                            name:"name",
                                            fileName:"track\(track.trackDate)",
                                            mimeType:"application/gpx")
                                    }
                                }, success: { (op:AFHTTPRequestOperation!, obj:AnyObject!) -> Void in
                                    self.presentAlert(string_success, message: "Successfully uploaded!")
                                }, failure: { (op: AFHTTPRequestOperation?, error: NSError) -> Void in
                                    self.presentAlert(string_error, message: error.localizedDescription)
                            })
                        }) { (error: NSError!) -> Void in
                            self.presentAlert(NSLocalizedString("Error", comment: "alert title"), message: error!.localizedDescription)
                    }
            })
            isObserved = true
        }
        
        // 3 calculate final url
        let params = "?scope=\(scope)&redirect_uri=\(redirect_uri)&client_id=\(clientID)&grant_type=password"
        // 4 open an external browser
//        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.google.com")!)
        let completeAuthURLString = "\(authorizeURLString)\(params)"
        print(completeAuthURLString)
        UIApplication.sharedApplication().openURL(NSURL(string: completeAuthURLString)!)
    }
    
    //MARK: UITableView
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let track = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Track {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let feedbackVC = storyboard.instantiateViewControllerWithIdentifier("FeedbackViewController") as! FeedbackViewController
            feedbackVC.track = track
            self.navigationController?.pushViewController(feedbackVC, animated: true)
        }
    }
    
    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let track = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Track
        // Populate cell from the NSManagedObject instance
//        print("Object for configuration: \(track.trackDate)")
        if let cell = cell as? TrackTableViewCell {
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.originLabel.text = track.origin
            cell.destinationLabel.text = track.destination
            
            if let distance = track.distance {
                let lenghtFormater = NSLengthFormatter()
                let numberFormatter = NSNumberFormatter()
                numberFormatter.minimumFractionDigits = 1
                numberFormatter.maximumFractionDigits = 1
                lenghtFormater.numberFormatter = numberFormatter
                cell.distanceLabel.text = lenghtFormater.stringFromMeters(distance.doubleValue)
            }
            
            if let trackDuration = track.duration {
                let dateFormater = NSDateComponentsFormatter()
                dateFormater.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
                dateFormater.unitsStyle = NSDateComponentsFormatterUnitsStyle.Abbreviated
                if let travelTimeString = dateFormater.stringFromTimeInterval(trackDuration.doubleValue) {
                    cell.timeLabel.text = travelTimeString
                }
            }
            
            if let date = track.trackDate {
                let formatter = NSDateFormatter()
                formatter.dateStyle = .ShortStyle
                formatter.timeStyle = .ShortStyle
                formatter.doesRelativeDateFormatting = true
                cell.dateLabel.text = formatter.stringFromDate(date)
            }
            
            if let trackPoints = track.trackPoints {
                var coordinates = [CLLocationCoordinate2D]()
                for trackPoint in trackPoints {
                    if let lat = (trackPoint as? TrackPoint)?.lat?.doubleValue, lon = (trackPoint as? TrackPoint)?.lon?.doubleValue {
                        coordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lon))
                    }
                }
                let line = MGLPolyline.init(coordinates: &coordinates, count: UInt(coordinates.count))
                cell.mapView.addAnnotation(line)
                let inset = CGFloat(10)
                cell.mapView.setVisibleCoordinates(&coordinates, count: UInt(coordinates.count), edgePadding: UIEdgeInsetsMake(inset, inset, inset, inset), animated: false)
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("trackCell", forIndexPath: indexPath)
        // Set up the cell
        self.configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let count = self.fetchedResultsController.sections?.count {
            return count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let sendAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: NSLocalizedString("Send", comment: "button title")) { (action, indexPath) -> Void in
            print("action tapped", indexPath)
            let track = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Track
            self.sendTrack(track)
        }
        sendAction.backgroundColor = UIColor.blueColor()
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Destructive, title: NSLocalizedString("Delete", comment: "button title")) { (action, indexPath) -> Void in
            print("delete tapped", indexPath)
            
            let track = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Track
            DataController.sharedInstance.managedObjectContext.deleteObject(track)
        }
        return [deleteAction, sendAction]
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            //delete row
            print("delete editingStyle", indexPath)
        }
    }
    
    //MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Move:
            break
        case .Update:
            break
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(self.tableView.cellForRowAtIndexPath(indexPath!)!, indexPath: indexPath!)
        case .Move:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            self.tableView.insertRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
}