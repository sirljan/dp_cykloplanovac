//
//  FavouritesViewController.swift
//  Cykloplanovac
//
//  Created by Jenda on 09/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import Foundation
import UIKit
import ContactsUI
import CoreData

let string_Favourites = NSLocalizedString("Favourites", comment: "title")
let string_Locations = NSLocalizedString("Locations", comment: "title")
let string_Contacts = NSLocalizedString("Contacts", comment: "title")

let standardMargin = 8.0

@available(iOS 9.0, *)
class FavouritesViewController: UITableViewController, CNContactPickerDelegate, NSFetchedResultsControllerDelegate {

    var segmentedControl:UISegmentedControl?
    var delegate:FavouritesViewControllerDelegate?
    var fetchedResultsController: NSFetchedResultsController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let segmentedControl = UISegmentedControl(items: [string_Locations, string_Contacts])
        segmentedControl.frame = CGRectMake(
            segmentedControl.frame.origin.x,
            segmentedControl.frame.origin.y,
            min(self.view.frame.size.width, self.view.frame.size.width) - CGFloat(2*standardMargin),
            segmentedControl.frame.size.height)
        
        let segItem = UIBarButtonItem(customView: segmentedControl)
        let spaceItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        self.toolbarItems = [spaceItem, segItem, spaceItem]
        
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: "segmentedControlValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        self.segmentedControl = segmentedControl
        
        self.initializeFetchedResultsController()
    }
    
    func initializeFetchedResultsController() {
        //todo rewrite for favourite place
        let request = NSFetchRequest(entityName: NSStringFromClass(NavCoordinate))
        request.predicate = NSPredicate(format: "coordinateTypeRaw == %@ OR coordinateTypeRaw == %@", NavCoordinateType.origin.rawValue, NavCoordinateType.destination.rawValue)
        let jorneySort = NSSortDescriptor(key: "address", ascending: true)
        request.sortDescriptors = [jorneySort]
        
        let moc = DataController.sharedInstance.managedObjectContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: "rootCache")
        self.fetchedResultsController.delegate = self
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func segmentedControlValueChanged(segmentedControl:UISegmentedControl) {
        switch(segmentedControl.selectedSegmentIndex) {
        case 0:
            //Favourites
            self.navigationController?.popToRootViewControllerAnimated(false)
        case 1:
            //Contacts
            let contactPickerViewController = CNContactPickerViewController()
            contactPickerViewController.delegate = self
            contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "postalAddresses.@count > 0")
            contactPickerViewController.predicateForSelectionOfContact = NSPredicate(format: "postalAddresses.@count == 1")
            contactPickerViewController.displayedPropertyKeys = [CNContactPostalAddressesKey]
            self.presentViewController(contactPickerViewController, animated: true, completion: nil)
        default:
            self.navigationController?.popToRootViewControllerAnimated(false)
        }
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didSelectAdress(postalAddress:CNPostalAddress) {
        delay(0.01, closure: { () -> () in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.delegate?.favouritesViewController(self, didSelectAddress: "\(postalAddress.street), \(postalAddress.city), \(postalAddress.country), \(postalAddress.postalCode)")
            })
        })
    }
    
    func didSelectAdress(postalAddressString postalAddress:String) {
        delay(0.01, closure: { () -> () in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.delegate?.favouritesViewController(self, didSelectAddress: postalAddress)
            })
        })
    }
    
    @IBAction func doneTapped(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: CNContactPickerDelegate
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContactProperty contactProperty: CNContactProperty) {
        if let postalAddress = contactProperty.value as? CNPostalAddress {
            self.didSelectAdress(postalAddress)
        }
        else
        {
            NSLog("ERROR: No postalAddress")
        }
    }
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        if let postalAddress = contact.postalAddresses[0].value as? CNPostalAddress {
            self.didSelectAdress(postalAddress)
        }
        else
        {
            NSLog("ERROR: No postalAddress")
        }
    }
    
    func contactPickerDidCancel(picker: CNContactPickerViewController) {
        self.segmentedControl?.selectedSegmentIndex = 0
    }
    
    //MARK: UITableViewDelegate, UITableViewDataSource
    
    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let object = self.fetchedResultsController.objectAtIndexPath(indexPath) as! NavCoordinate
        // Populate cell from the NSManagedObject instance
        if let address = object.address {
            cell.textLabel?.text = address
        }
        if let coordinate = object.locationCoordinate2D {
            cell.detailTextLabel?.text = "lat: \(coordinate.latitude), lon: \(coordinate.longitude)"
        }
        let isEmpty = (cell.textLabel?.text == nil || cell.textLabel?.text != nil && cell.textLabel!.text!.isEmpty)
        cell.textLabel?.text = isEmpty ? NSLocalizedString("Unknown location address", comment: "error text") : cell.textLabel?.text
        
        print("Object for configuration: \(object.address)")
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("favouritePlace", forIndexPath: indexPath)
        // Set up the cell
        self.configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let count = self.fetchedResultsController.sections!.count
        return count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.fetchedResultsController.sections {
            let sectionInfo = sections[section]
            let rows = sectionInfo.numberOfObjects
            return rows
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let navCoordinate = self.fetchedResultsController.objectAtIndexPath(indexPath) as! NavCoordinate
        if let address = navCoordinate.address {
            self.didSelectAdress(postalAddressString: address)
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            //delete row
            print("delete editingStyle", indexPath)
            
            let navCoordinate = self.fetchedResultsController.objectAtIndexPath(indexPath) as! NavCoordinate
            DataController.sharedInstance.managedObjectContext.deleteObject(navCoordinate)
        }
    }
    
    //MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Move:
            break
        case .Update:
            break
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(self.tableView.cellForRowAtIndexPath(indexPath!)!, indexPath: indexPath!)
        case .Move:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            self.tableView.insertRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
}