//
//  FeedbackViewController.swift
//  Cykloplanovac
//
//  Created by Jan Širl on 26/12/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit
import Mapbox

class FeedbackViewController: UIViewController, UITextViewDelegate, MGLMapViewDelegate{
    
    var delegate:FeedbackViewControllerDelegate?
    var track:Track?
    
    @IBOutlet weak var mapWrapperView: UIView!
    var mapView:MGLMapView!
    @IBOutlet weak var star1Button: UIButton!
    @IBOutlet weak var star2Button: UIButton!
    @IBOutlet weak var star3Button: UIButton!
    @IBOutlet weak var star4Button: UIButton!
    @IBOutlet weak var star5Button: UIButton!
    var starButtons:[UIButton] = []
    var rating:Int = 0
    @IBOutlet weak var commentTextView: UITextView!
    var activeTextInput:AnyObject?
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var insideView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var averageSpeedLabel: UILabel!
    
    var trackPolyLine:MGLAnnotation?
    var planPolyLine:MGLAnnotation?
    
    func starButtonTapped(starButton:UIButton) {
        var highlight = false
        for button in starButtons.reverse() {
            if button === starButton || highlight {
                highlight = true
                let image = UIImage(named: "starFilled.png")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                button.setImage(image, forState: UIControlState.Normal)
                let color = button.tintColor
                button.tintColor = color
            } else {
                let image = UIImage(named: "starUnfilled.png")
                button.setImage(image, forState: UIControlState.Normal)
            }
        }
    }
    
    @IBAction func star1Tapped (sender: UIButton) {
        self.starButtonTapped(sender)
        self.rating = 1
    }
    @IBAction func star2Tapped(sender: UIButton) {
        self.starButtonTapped(sender)
        self.rating = 2
    }
    @IBAction func star3Tapped(sender: UIButton) {
        self.starButtonTapped(sender)
        self.rating = 3
    }
    @IBAction func star4Tapped(sender: UIButton) {
        self.starButtonTapped(sender)
        self.rating = 4
    }
    @IBAction func star5Tapped(sender: UIButton) {
        self.starButtonTapped(sender)
        self.rating = 5
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        self.view.endEditing(false)
        
        let activityView = UIActivityIndicatorView(frame: CGRectMake(0,0,20,20))
        activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        let activityButton = UIBarButtonItem(customView: activityView)
        self.navigationItem.rightBarButtonItem = activityButton
        activityView.startAnimating()
        
        self.track?.textualFeedback = self.commentTextView.text
        self.track?.rating = NSNumber(integer: self.rating)
        
        if let track = track {
            BackendCommunicator.sharedInstance.postFeedBack(track) {
                (errorMessage:String?) in
                if let errorMessage = errorMessage {
                    self.presentAlert(string_error, message: errorMessage)
                } else {
                    //success
                    self.dismissViewControllerAnimated(true, completion:nil)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.commentTextView.layer.borderWidth = 1
        self.commentTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.commentTextView.layer.cornerRadius = 5
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.barTintColor = UIColor.whiteColor()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "resignKeyboard")
        toolbar.items = [flexibleSpace, doneButton]
        self.commentTextView.inputAccessoryView = toolbar
        
        starButtons = [star1Button, star2Button, star3Button, star4Button, star5Button]
        
        let mapView:MGLMapView
        mapView = MGLMapView(frame: self.mapWrapperView.bounds)
        mapView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.mapWrapperView.addSubview(mapView)
        self.mapView = mapView
        self.mapView.delegate = self
        
        var trackCoordinates = [CLLocationCoordinate2D]()
        if let trackPoints = self.track?.trackPoints {
            for trackPoint in trackPoints {
                if let lat = (trackPoint as? TrackPoint)?.lat?.doubleValue, lon = (trackPoint as? TrackPoint)?.lon?.doubleValue {
                    trackCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lon))
                }
            }
            let line = MGLPolyline.init(coordinates: &trackCoordinates, count: UInt(trackCoordinates.count))
            self.trackPolyLine = line
            self.mapView.addAnnotation(line)
            let inset = CGFloat(10)
            self.mapView.setVisibleCoordinates(&trackCoordinates, count: UInt(trackCoordinates.count), edgePadding: UIEdgeInsetsMake(inset, inset, inset, inset), animated: false)
        }
        
//        var coordinates = [CLLocationCoordinate2D]()
//        if let plan = self.track?.plan, steps = plan.steps {
//            for step in steps {
//                coordinates.append(CLLocationCoordinate2D(latitude: step.coordinate.latitude, longitude: step.coordinate.longitude))
//            }
//            let line = MGLPolyline.init(coordinates: &coordinates, count: UInt(coordinates.count))
//            self.planPolyLine = line
//            self.mapView.addAnnotation(line)
//        }
//        coordinates = coordinates + trackCoordinates
//        let inset = CGFloat(10)
//        self.mapView.setVisibleCoordinates(&coordinates, count: UInt(coordinates.count), edgePadding: UIEdgeInsetsMake(inset, inset, inset, inset), animated: false)
        
        //dashboard
        if let distance = self.track?.distance {
            let lenghtFormater = NSLengthFormatter()
            let numberFormatter = NSNumberFormatter()
            numberFormatter.minimumFractionDigits = 1
            numberFormatter.maximumFractionDigits = 1
            lenghtFormater.numberFormatter = numberFormatter
            self.distanceLabel.text = lenghtFormater.stringFromMeters(distance.doubleValue)
        }
        if let travelTime = self.track?.duration {
            let dateFormater = NSDateComponentsFormatter()
            dateFormater.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute]
            dateFormater.unitsStyle = NSDateComponentsFormatterUnitsStyle.Short
            if let travelTimeString = dateFormater.stringFromTimeInterval(travelTime.doubleValue) {
                self.durationLabel.text =  travelTimeString
            }
        }
        if let durationInSec = self.track?.duration {
            let durationInHours = durationInSec.doubleValue / 3600
            if let distanceInMeters = self.track?.distance?.doubleValue {
                let speed = distanceInMeters / durationInHours
                
                let lenghtFormater = NSLengthFormatter()
                let numberFormatter = NSNumberFormatter()
                numberFormatter.minimumFractionDigits = 1
                numberFormatter.maximumFractionDigits = 1
                lenghtFormater.numberFormatter = numberFormatter
                self.averageSpeedLabel.text = "\(lenghtFormater.stringFromMeters(speed)) / \(NSLocalizedString("h", comment: "abbreviation for hours"))"
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK: - Keyboard Management Methods
    
    func resignKeyboard() {
        self.view.endEditing(false)
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        var visibleRect: CGRect = self.insideView.frame
        visibleRect.size.height -= keyboardSize.height
        if let activeTextInputRect: CGRect = activeTextInput?.frame {
            let contains = CGRectContainsRect(visibleRect, activeTextInputRect)
            if (!contains) {
                scrollView.scrollRectToVisible(activeTextInputRect, animated:true)
            }
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    //MARK: - UITextField Delegate Methods
    
    func textFieldDidBeginEditing(textField: UITextField!) {
        self.activeTextInput = textField
        scrollView.scrollEnabled = true
    }
    
    func textFieldDidEndEditing(textField: UITextField!) {
        activeTextInput = nil
        scrollView.scrollEnabled = false
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        self.activeTextInput = textView
        scrollView.scrollEnabled = true
        return true
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.activeTextInput = textView
        scrollView.scrollEnabled = true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        activeTextInput = nil
        scrollView.scrollEnabled = false
    }
    
    //MARK: MapView Delegate
    
    func mapView(mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        if annotation === self.trackPolyLine {
            return UIColor.blueColor()
        }
        if annotation === self.planPolyLine {
            return UIColor.orangeColor()
        }
        return UIColor.blackColor()
    }
}
