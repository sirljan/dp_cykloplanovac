//
//  ViewController.swift
//  Cykloplanovac
//
//  Created by Jenda on 18/07/15.
//  Copyright (c) 2015 FEL CVUT. All rights reserved.
//

import UIKit
import Mapbox
import CoreData

let string_signIn       = NSLocalizedString("Sign in", comment: "button, shows login textfields")
let string_login        = NSLocalizedString("Login", comment: "button, placeholder or other text")
let string_password     = NSLocalizedString("Password", comment: "button title or placeholder or other text")
let string_cancel       = NSLocalizedString("Cancel", comment: "Cancel button")

let string_Start        = NSLocalizedString("Start", comment: "title for origin of route")
let string_Destination  = NSLocalizedString("Destination", comment: "title for end of route")

enum CykloplanovacMode {
    case Map
    case Planning
    case Navigating
    case Recording
}

func stringFromUserTrackMode(mode:MGLUserTrackingMode) -> String {
    let array = [
    "None",
    "Follow",
    "FollowWithHeading",
    "FollowWithCourse"
    ]
    let num:NSNumber = NSNumber.init(unsignedInteger: mode.rawValue)
    return array[num.integerValue]
}

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

protocol PlanViewControllerDelegate {
    func planViewControllerPlanJourney(fromOrigin origin:CLLocationCoordinate2D, toDestination destination:CLLocationCoordinate2D, originAddress:String, destinationAddress:String)
    func planViewControllerPlanJourney(journey:Journey)
    func planViewControllerUserLocation() -> CLLocationCoordinate2D?
    func planViewControllerStartAddress() -> String?
    func planViewControllerEndAddress() -> String?
    func planViewControllerUser() -> User
}

protocol FeedbackViewControllerDelegate {
    
}

let client = "ios_beta"

@available(iOS 9.0, *)
class ViewController: UIViewController, MGLMapViewDelegate, PlanViewControllerDelegate, PlanDetailViewControllerDelegate, FeedbackViewControllerDelegate {

    let moc:NSManagedObjectContext
    
    var user:User
    var mapView: MGLMapView!
    var planDetailViewController:PlanDetailViewController?
    
    var userTrackingMode:MGLUserTrackingMode = MGLUserTrackingMode.Follow
    
    var mode:CykloplanovacMode = CykloplanovacMode.Map {
        didSet {
            switch mode {
            case CykloplanovacMode.Recording:
                self.mapView.userTrackingMode = .Follow
            default: break
            }
        }
    }
    weak var activityView:UIView?
    
    @IBOutlet weak var trackModeButton: UIButton!
    @IBOutlet weak var trackButtonBackgroundView: UIView!
    @IBOutlet var planButton: UIBarButtonItem!
    @IBOutlet weak var planDetailWrapperView: UIView!
    
    var originTuple:(navCoordinate: NavCoordinate, marker: MGLAnnotation)?
    var destinationTuple:(navCoordinate: NavCoordinate, marker: MGLAnnotation)?
    var currentJourney:Journey?
    var selectedPlan:Plan?
    var standartToolbarItems: [UIBarButtonItem]?
    var trackHandler:TrackHandler?
    
    required init?(coder aDecoder: NSCoder) {
        moc = DataController.sharedInstance.managedObjectContext
        let entityFetchRequest = NSFetchRequest(entityName: NSStringFromClass(User))
        do {
            let fetchedEntities = try moc.executeFetchRequest(entityFetchRequest) as! [User]
            if let entity = fetchedEntities.first {
                //load entity
                self.user = entity
                print("fetched user.name \(self.user.name)")
            } else {
                self.user = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(User), inManagedObjectContext: moc) as! User
//                self.user.name = "test user"
                self.user.work = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(NavCoordinate), inManagedObjectContext: moc) as? NavCoordinate
//                self.user.work?.address = "Na Strzi 1702, Praha 4"
                self.user.home = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(NavCoordinate), inManagedObjectContext: moc) as? NavCoordinate
//                self.user.home?.address = "Secska 9, Praha 10"
                self.user.tracks = NSOrderedSet()
                //save entity
                DataController.sharedInstance.saveContext()
            }
            
        } catch {
            fatalError("Failed to fetch entity: \(error)")
        }
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initialize the map view
        self.mapView = MGLMapView(frame: view.bounds)
        self.mapView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.mapView.pitchEnabled = false
        self.mapView.rotateEnabled = false
        // set the map's center coordinate
        self.mapView.setCenterCoordinate(CLLocationCoordinate2D(latitude:50.083692,longitude: 14.424019), zoomLevel: 12, animated: false)
        view.addSubview(self.mapView)
        self.view.sendSubviewToBack(self.mapView)
        // Set the delegate property of our map view to self after instantiating it.
        self.mapView.delegate = self
        
        self.mode = CykloplanovacMode.Map
        self.trackButtonBackgroundView.layer.cornerRadius = self.trackButtonBackgroundView.bounds.size.width/2
        self.trackButtonBackgroundView.superview?.hidden = true
        
        let longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        self.mapView.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hidePlanDetailView(hidden: true, animated: false, completionHandler: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.trackButtonBackgroundView.superview?.alpha = 0
        self.trackButtonBackgroundView.superview?.hidden  = false
        UIView.animateWithDuration(0.3, delay: 1.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.trackButtonBackgroundView.superview?.alpha = 1
            }, completion: nil)
        mapView.showsUserLocation = true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PlanViewController" {
            let navigationController = segue.destinationViewController as? UINavigationController
            let planViewController = navigationController?.topViewController as? PlanViewController
            planViewController?.delegate = self
        }
    }
    
    @IBAction func hideTapped(sender: AnyObject) {
        self.hidePlanDetailView(hidden: nil, animated: true, completionHandler: nil)
    }
    
    func displayError(erroTitle: String, errorMessage: String) {
        let alertController = UIAlertController(title: erroTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title:NSLocalizedString("OK", comment: "alert button title"), style: UIAlertActionStyle.Cancel) { (alertAction:UIAlertAction) -> Void in
        }
        alertController.addAction(action)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func hidePlanDetailView(hidden hiddenOpt:Bool?, animated:Bool?, completionHandler:(()->Void)?) {
        dispatch_async(dispatch_get_main_queue(), {
            let hiddenPriority = Float(999)
            let showPriority = Float(250)
            if let animated = animated {
                var hidden:Bool = false
                if let hiddenOpt = hiddenOpt {
                    hidden = hiddenOpt
                } else {
                    hidden = !self.planDetailWrapperView.hidden
                }
                UIView.animateWithDuration((animated ? 0.3 : 0), animations: { () -> Void in
                    self.planDetailWrapperView.alpha = hidden ? 0 : 1
                    }, completion: { (finished) -> Void in
                        self.planDetailWrapperView.hidden = hidden
                        for subview in self.planDetailWrapperView.subviews {
                            for constraint in subview.constraints {
                                constraint.active = !hidden
                            }
                        }
                        for constraint:NSLayoutConstraint in self.planDetailWrapperView.constraints where constraint.identifier == "hideHeightContraint" {
                            constraint.priority = hidden ? hiddenPriority : showPriority
                        }
                        completionHandler?()
                })
            } else {
                if let hidden = hiddenOpt {
                    self.planDetailWrapperView.hidden = hidden
                } else {
                    self.planDetailWrapperView.hidden = !self.planDetailWrapperView.hidden
                }
                
                for subview in self.planDetailWrapperView.subviews {
                    for constraint in subview.constraints {
                        constraint.active = !self.planDetailWrapperView.hidden
                    }
                }
                for constraint:NSLayoutConstraint in self.planDetailWrapperView.constraints where constraint.identifier == "hideHeightContraint" {
                    constraint.priority = self.planDetailWrapperView.hidden ? hiddenPriority : showPriority
                }
                
                completionHandler?()
            }
        })
    }
    
    func showPlanDetailViewController(journey:Journey) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let planDetailViewController = storyboard.instantiateViewControllerWithIdentifier("PlanDetailViewController") as! PlanDetailViewController
        self.planDetailViewController = planDetailViewController
        self.addChildViewController(planDetailViewController)
        planDetailViewController.delegate = self
        planDetailViewController.journey = journey
        self.hidePlanDetailView(hidden: false, animated: true, completionHandler: nil)
        planDetailViewController.view.frame = self.planDetailWrapperView.bounds
        planDetailViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        self.standartToolbarItems = self.toolbarItems
        dispatch_async(dispatch_get_main_queue(), {
            self.planDetailWrapperView.addSubview(planDetailViewController.view)
            planDetailViewController.didMoveToParentViewController(self)
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancelJourneyTapped:")
            
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let navigateButton = UIBarButtonItem(title: NSLocalizedString("Navigate", comment: "button title, starts navigations"), style: UIBarButtonItemStyle.Done, target: self, action: "navigateButtonTapped:")
            self.setToolbarItems([space, navigateButton, space], animated: true)
        })
    }
    
    func showFeedBackViewController(track:Track) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let feedbackNC = storyboard.instantiateViewControllerWithIdentifier("FeedbackNavigationController") as? UINavigationController, feedbackViewController = feedbackNC.topViewController as? FeedbackViewController {
            feedbackViewController.track = track
            self.presentViewController(feedbackNC, animated: true, completion: nil)
        }
    }
    
    func dismissPlanDetailViewController() {
        self.hidePlanDetailView(hidden: true, animated: true, completionHandler: {
            self.planDetailViewController?.removeFromParentViewController()
            self.planDetailViewController?.view.removeFromSuperview()
            self.setToolbarItems(self.standartToolbarItems, animated: true)
        })
    }
    
    func uploadGPX(track:Track) {
        self.trackHandler?.gpxString()
        
        self.trackHandler = nil
    }
    
    func setImageUserTrackingModeButton() {
        let image:UIImage?
        switch self.userTrackingMode {
        case MGLUserTrackingMode.None:
            image = UIImage(named: "trackingModeNone.png")
        case MGLUserTrackingMode.Follow:
            image = UIImage(named: "trackingModeFollow.png")
        case MGLUserTrackingMode.FollowWithHeading:
            image = UIImage(named: "trackingModeFollowHeading.png")
        case MGLUserTrackingMode.FollowWithCourse:
            image = UIImage(named: "trackingModeFollowCourse.png")
        }
        self.trackModeButton.setImage(image, forState: UIControlState.Normal)
    }
    
    //MARK: Buttons
    
    func navigateButtonTapped(sender:AnyObject?) {
        
        self.dismissPlanDetailViewController()
        self.removeUnselectedPlans()
        let duration = 0.4
        if let coordinate = self.mapView.userLocation?.location?.coordinate {
            let camera = MGLMapCamera(lookingAtCenterCoordinate: coordinate, fromDistance: 2000, pitch: 0, heading: 0)
            self.mapView.setCamera(camera, withDuration: duration, animationTimingFunction: nil)
        }
        delay(duration) { () -> () in
            self.mapView.userTrackingMode = MGLUserTrackingMode.FollowWithCourse
        }
        self.mode = CykloplanovacMode.Navigating
        if let selectedPlan = self.selectedPlan {
            self.trackHandler = TrackHandler(plan: selectedPlan, user: self.user)
        }
    }
    
    @IBAction func feedbackTapped(sender: AnyObject) {
        if let track = self.user.tracks?.lastObject as? Track {
            self.showFeedBackViewController(track)
        } else {
            self.presentAlert(string_error, message: "No last track")
        }
    }
    
    @IBAction func recordTrackButtonTapped(sender: UIBarButtonItem) {
        if self.mode == CykloplanovacMode.Map {
            self.mode = CykloplanovacMode.Recording
            sender.title = NSLocalizedString("Stop", comment: "button title, stop recording track")
            self.trackHandler = TrackHandler(plan: nil, user: self.user)
            
        } else if self.mode == CykloplanovacMode.Recording {
            self.mode = CykloplanovacMode.Map
            sender.title = NSLocalizedString("Record Track", comment: "button title, start recording track")
            if let track = trackHandler?.track {
                self.uploadGPX(track)
            }
        }
    }
    
    @IBAction func trackModeButtonTapped(sender: UIButton) {
        self.userTrackingMode = MGLUserTrackingMode(rawValue: (self.userTrackingMode.rawValue + 1) % 4)!
        self.mapView.userTrackingMode = self.userTrackingMode
        self.setImageUserTrackingModeButton()
    }
    
    @IBAction func testJourneyButtonTapped(sender: AnyObject) {
        self.clearMap()
        
        let originLocationCoordinate2D = CLLocationCoordinate2DMake(50.083692, 14.424019)
        let destinationLocationCoordinate2D = CLLocationCoordinate2DMake(50.074824, 14.437666)
        
        self.zoomToLocations(originLocationCoordinate2D, destinationCoordinate: destinationLocationCoordinate2D)
        
        delay(0.5) { () -> () in
            var marker = MGLPointAnnotation()
            marker.coordinate = originLocationCoordinate2D
            marker.title = string_Start
            self.mapView.addAnnotation(marker)
            marker = MGLPointAnnotation()
            marker.coordinate = destinationLocationCoordinate2D
            marker.title = string_Destination
            self.mapView.addAnnotation(marker)
            
            self.planJourney(nil, destinationAddress: nil)
        }
    }
    
    @IBAction func loadJourneyButtonTapped(sender: UIBarButtonItem) {
        let entityFetch = NSFetchRequest(entityName: NSStringFromClass(Journey))
        do {
            let fetchedEntities = try moc.executeFetchRequest(entityFetch) as! [Journey]
            if let entity:Journey = fetchedEntities.last {
                //load entity
                let journey:Journey = entity
                if let origin = journey.origin?.locationCoordinate2D, destination = journey.destination?.locationCoordinate2D {
                    self.addMarkers(originCoordinate: origin, destinationCoordinate: destination)
                    self.zoomToJourney(journey)
                    self.addPlanningActivityIndicator()
                    self.drawJourney(journey)
                }
            }
        } catch {
            fatalError("Failed to fetch entity: \(error)")
        }
    }
    
    func cancelJourneyTapped(sender: UIBarButtonItem) {
        self.removePlanningActivityIndicator()
        self.navigationItem.leftBarButtonItem = self.planButton
        self.clearMap()
        self.dismissPlanDetailViewController()
        self.mapView.userTrackingMode = MGLUserTrackingMode.None
        self.currentJourney = nil
        self.selectedPlan = nil
        self.trackHandler = nil
        self.mode = CykloplanovacMode.Map
        self.originTuple = nil
        self.destinationTuple = nil
    }
    
    func handleLongPressGesture(gesture:UILongPressGestureRecognizer) {
        //MGLPointAnnotation
        let markersCount = self.annotationsOfKind(MGLPointAnnotation)?.count
        if gesture.state == UIGestureRecognizerState.Recognized && (self.mapView.annotations == nil || markersCount < 2) {
            let point = gesture.locationInView(self.mapView)
            
            let locationCoordinate = self.mapView.convertPoint(point, toCoordinateFromView: self.mapView)
            if self.mapView.annotations == nil || markersCount == 0 || self.destinationTuple != nil {
                self.addMarkers(originCoordinate: locationCoordinate, destinationCoordinate: nil)
            } else  {
                self.addMarkers(originCoordinate: nil, destinationCoordinate: locationCoordinate)
            }
            
            self.planJourney(nil, destinationAddress: nil)
        }
    }
    
    // MARK: Working with MapView
    
    func zoomToJourney(journey:Journey) {
        var coords: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        if let plans = journey.plans {
            for plan in plans {
                if let plan = plan as? Plan {
                    if let coord = plan.boundingBox?.topLeftCoordinate {
                        coords.append(coord)
                    }
                    if let coord = plan.boundingBox?.bottomRightCoordinate {
                        coords.append(coord)
                    }
                }
            }
            
            self.setVisibleCoordinates(&coords, count: UInt(coords.count))
        }
    }
    
    func zoomToPlan(plan:Plan) {
        var coords: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        if let coord = plan.boundingBox?.topLeftCoordinate {
            coords.append(coord)
        }
        if let coord = plan.boundingBox?.bottomRightCoordinate {
            coords.append(coord)
        }

        self.setVisibleCoordinates(&coords, count: UInt(coords.count))
    }
    
    func setVisibleCoordinates(coordinates: UnsafeMutablePointer<CLLocationCoordinate2D>, count: UInt) {
        let inset = max(self.view.bounds.size.height, self.view.bounds.size.width) * 0.1
        let insets = UIEdgeInsetsMake(inset + 44, inset, inset + 44, inset)
        self.mapView.setVisibleCoordinates(coordinates, count: count, edgePadding: insets, animated: true)
    }
    
    func zoomToLocations(originCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
        var coords: [CLLocationCoordinate2D] = [originCoordinate, destinationCoordinate]
        self.setVisibleCoordinates(&coords, count: UInt(coords.count))
    }
    
    func addPlanningActivityIndicator() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancelJourneyTapped:")
        if self.activityView == nil {
            let activityView = UIView(frame: self.view.bounds)
            activityView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            activityView.backgroundColor = UIColor.whiteColor()
            activityView.alpha = 0.0
            self.activityView = activityView
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.startAnimating()
            activityView.addSubview(activityIndicator)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.centerXAnchor.constraintEqualToAnchor(activityView.centerXAnchor).active = true
            activityIndicator.centerYAnchor.constraintEqualToAnchor(activityView.centerYAnchor).active = true
            self.view.addSubview(activityView)
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                activityView.alpha = 0.5
            })
        }
    }
    
    func removePlanningActivityIndicator() {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityView?.removeFromSuperview()
            self.activityView = nil
        })
    }
    
    func addMarkers(originCoordinate originCoordinate:CLLocationCoordinate2D?, destinationCoordinate:CLLocationCoordinate2D?) {
        if let originCoordinate = originCoordinate {
            let marker = MGLPointAnnotation()
            marker.coordinate = originCoordinate
            marker.title = string_Start
            
            let origin = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(NavCoordinate), inManagedObjectContext: moc) as? NavCoordinate
            if let origin = origin {
                origin.setup(originCoordinate, type: NavCoordinateType.origin)
                self.originTuple = (origin, marker)
            }
            
            mapView.addAnnotation(marker)
        }
        if let destinationCoordinate = destinationCoordinate {
            let marker = MGLPointAnnotation()
            marker.coordinate = destinationCoordinate
            marker.title = string_Destination
            
            let destination = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(NavCoordinate), inManagedObjectContext: moc) as? NavCoordinate
            if let destination = destination {
                destination.setup(destinationCoordinate, type: NavCoordinateType.destination)
                self.destinationTuple = (destination, marker)
            }
            
            mapView.addAnnotation(marker)
        }
    }
    
    func clearMap() {
        if let lines = self.annotationsOfKind(MGLPolyline) {
            self.mapView.removeAnnotations(lines)
        }
        if let markers = self.annotationsOfKind(MGLPointAnnotation) {
            self.mapView.removeAnnotations(markers)
        }
    }
    
    func removeUnselectedPlans() {
        if let plans = self.annotationsOfKind(MGLPolyline), selectedPlanId = self.selectedPlan?.planId?.stringValue {
            for plan in plans {
                if let planTitle = plan.title {
                    if  planTitle != selectedPlanId {
                        self.mapView.removeAnnotation(plan)
                    }
                }
            }
        }
    }
    
    func annotationsOfKind(aClass: AnyClass) -> Array<MGLAnnotation>? {
        var array = [MGLAnnotation]()
        if let annotations = self.mapView.annotations {
            for annotation in annotations {
                if annotation.isKindOfClass(aClass) {
                    array.append(annotation)
                }
            }
        }
        return array
    }
    
    
    
    func planJourney(originAddress:String?, destinationAddress:String?) {
        let origin = self.originTuple?.navCoordinate
        let destination = self.destinationTuple?.navCoordinate
        
        if let originCoordinate = origin?.locationCoordinate2D, destinationCoordinate = destination?.locationCoordinate2D {
            if self.mode != CykloplanovacMode.Navigating {
                self.zoomToLocations(originCoordinate, destinationCoordinate: destinationCoordinate)
            }
        }
        
        //set address
        if let originAddress = originAddress {
            origin?.address = originAddress
        } else {
            Geocoder.geocodeLocation(origin?.locationCoordinate2D, completion: { (address, error) -> Void in
                origin?.address = address
            })
        }
        if let destinationAddress = destinationAddress {
            destination?.address = destinationAddress
        } else {
            Geocoder.geocodeLocation(destination?.locationCoordinate2D, completion: { (address, error) -> Void in
                destination?.address = address
            })
        }
        
        if let origin = origin, destination = destination {
            if self.mode != CykloplanovacMode.Navigating {
                self.addPlanningActivityIndicator()
            }
            
            let moc = DataController.sharedInstance.managedObjectContext
            // we set up our entity by selecting the entity and context that we're targeting
            let journey = NSEntityDescription.insertNewObjectForEntityForName(NSStringFromClass(Journey), inManagedObjectContext: moc) as! Journey
            journey.setupJourney(origin, destination: destination, client:client)
            self.user.mutableOrderedSetValueForKey("journeys").addObject(journey)
            
            let backendCommunicator = BackendCommunicator.sharedInstance
            backendCommunicator.planJourney(journey) { (locationURL, errorMessage) -> Void in
                if let errorMessage = errorMessage {
                    self.presentAlert(string_error, message: errorMessage)
                    self.removePlanningActivityIndicator()
                } else {
                    backendCommunicator.getJourneyAtLocationURL(locationURL, completionHandler: { (dictionary, errorMessage) -> Void in
                        if let errorMessage = errorMessage {
                            self.presentAlert(string_error, message: errorMessage)
                            self.removePlanningActivityIndicator()
                        } else {
                            journey.mergeWithDictionary(dictionary)
                            self.drawJourney(journey)
                            //                    DataController.sharedInstance.saveContext()
                        }
                    })
                }
            }
        }
    }
    
    func drawJourney(journey:Journey) {
        self.currentJourney = journey
        if let plans = journey.plans {
            for plan in plans {
                if let plan = plan as? Plan {
                    var coordinates = [CLLocationCoordinate2D]()
                    if let steps = plan.steps {
                        for step in steps {
                            if let step = step as? Step, let coordinate = step.coordinate?.locationCoordinate2D {
                                coordinates.append(coordinate)
                            }
                        }
                    }
                    
                    let line = MGLPolyline.init(coordinates: &coordinates, count: UInt(coordinates.count))
                    line.title = plan.planId?.stringValue
                    self.mapView.addAnnotation(line)
                }
            }
        }
        if self.mode != CykloplanovacMode.Navigating {
            self.removePlanningActivityIndicator()
            self.showPlanDetailViewController(journey)
        }
    }
    
    //MARK: PlanDelegateViewControllerDelegate
    
    func planDetailViewControllerDidSelectPlan(plan: Plan?) {
        if let previousSelectedPlanId = self.selectedPlan?.planId?.stringValue {
            print("unselecting \(previousSelectedPlanId)")
            self.selectedPlan = nil
            self.reAddAnnotationWithTitle(previousSelectedPlanId)
        }
        if let selectedPlan = plan, selectedPlanId = selectedPlan.planId?.stringValue {
            print("selecting \(selectedPlan.planId?.stringValue)")
            self.selectedPlan = selectedPlan
            self.reAddAnnotationWithTitle(selectedPlanId)
        }
    }
    
    func reAddAnnotationWithTitle(title:String) {
        if let annotations = self.annotationsOfKind(MGLPolyline) {
            for annotation in annotations {
                if let annotationTitle = annotation.title {
                    if annotationTitle == title {
                        self.mapView.removeAnnotation(annotation)
                        self.mapView.addAnnotation(annotation)
                    }
                }
            }
        }
    }
    
    //MARK: PlanViewControllerDelegate
    
    func planViewControllerPlanJourney(fromOrigin originCoordinate:CLLocationCoordinate2D, toDestination destinationCoordinate:CLLocationCoordinate2D, originAddress:String, destinationAddress:String) {
        self.clearMap()
        self.zoomToLocations(originCoordinate, destinationCoordinate: destinationCoordinate)
        self.addMarkers(originCoordinate: originCoordinate, destinationCoordinate: destinationCoordinate)
        self.planJourney(nil, destinationAddress: nil)
    }
    
    func planViewControllerPlanJourney(journey:Journey) {
        self.clearMap()
        self.zoomToJourney(journey)
        self.addMarkers(originCoordinate: journey.origin?.locationCoordinate2D, destinationCoordinate: journey.destination?.locationCoordinate2D)
        self.drawJourney(journey)
    }
    
    func planViewControllerUserLocation() -> CLLocationCoordinate2D? {
        let coordinate = self.mapView.userLocation?.location?.coordinate
        return coordinate
    }
    
    func planViewControllerStartAddress() -> String? {
        return self.currentJourney?.origin?.address
    }
    
    func planViewControllerEndAddress() -> String? {
        return self.currentJourney?.destination?.address
    }
    
    func planViewControllerUser() -> User {
        return self.user
    }
    
    // MARK: MGLMapViewDelegate
    
    func mapView(mapView: MGLMapView, didSelectAnnotation annotation: MGLAnnotation) {
        self.mapView.removeAnnotation(annotation)
        if let annotations = self.annotationsOfKind(MGLPolyline) {
            self.mapView.removeAnnotations(annotations)
            self.dismissPlanDetailViewController()
        }
        if annotation === self.originTuple?.marker {
            self.originTuple = nil
        }
        if annotation === self.destinationTuple?.marker {
            self.destinationTuple = nil
        }
    }
    
    func mapView(mapView: MGLMapView, imageForAnnotation annotation: MGLAnnotation) -> MGLAnnotationImage? {
        if let originMarker = self.originTuple?.marker {
            if annotation === originMarker {
                let image = UIImage(named:"marker-stroked-24.png")
                return MGLAnnotationImage(image: image!, reuseIdentifier: "marker-stroked-24.png")
            }
        }
        
        if let destinationMarker = self.destinationTuple?.marker {
            if annotation === destinationMarker {
                let image = UIImage(named:"marker-24.png")
                return MGLAnnotationImage(image: image!, reuseIdentifier: "marker-24.png")
            }
        }

        return nil
    }
    
    func mapView(mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapViewWillStartLocatingUser(mapView: MGLMapView) {
//        print("mapViewWillStartLocatingUser")
    }
    
    func mapView(mapView: MGLMapView, didFailToLocateUserWithError error: NSError) {
        print("didFailToLocateUserWithError", terminator: "")
    }
    
    func mapViewDidStopLocatingUser(mapView: MGLMapView) {
        print("mapViewDidStopLocatingUser", terminator: "")
    }
    
    func mapView(mapView: MGLMapView, didChangeUserTrackingMode mode: MGLUserTrackingMode, animated: Bool) {
        if mode == MGLUserTrackingMode.Follow || mode == MGLUserTrackingMode.None {
            self.mapView.resetNorth()
        }
        self.userTrackingMode = mode
        self.setImageUserTrackingModeButton()
    }
    
    func mapView(mapView: MGLMapView, didUpdateUserLocation userLocation: MGLUserLocation?) {
        if let userLocationCoordinate = userLocation?.coordinate {
            if self.mode == CykloplanovacMode.Navigating {
                let currentStep = self.trackHandler?.addUserLocation(userLocationCoordinate)
                //update of detail view
                if let isOnTrack = self.trackHandler?.isUserOnTrack() where isOnTrack == false {
                    NSLog("isOnTrack false")

                    //todo
                    //plan journey with same origin and destination,
                    //select plan with same criteria
                    self.planJourney(nil, destinationAddress: nil)
                } else {
                    NSLog("isOnTrack true")
                }
                if let isUserInDestination = self.trackHandler?.isUserInDestination(userLocationCoordinate) where isUserInDestination == true {
                    if let track = self.user.tracks?.lastObject as? Track {
                        //show feedback todo
                        self.showFeedBackViewController(track)
                        //upload gpx
                        self.uploadGPX(track)
                    }
                    //finish navigation
                    self.mode = CykloplanovacMode.Map
                }
            }
            if self.mode == CykloplanovacMode.Recording {
                self.trackHandler?.addUserLocation(userLocationCoordinate)
            }
        }
    }
    
    func mapView(mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        if self.selectedPlan?.planId?.stringValue == annotation.title {
            return 1.0
        }
        return 0.5
    }
    
    func mapView(mapView: MGLMapView, lineWidthForPolylineAnnotation annotation: MGLPolyline) -> CGFloat {
        // Set the line width for polyline annotations
//        if let selectedPlan = self.selectedPlanId where selectedPlan == annotation.title {
//            return 4.0
//        }
        return 5.0
    }
    
    func mapView(mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        if self.selectedPlan?.planId?.stringValue == annotation.title {
            return UIColor.orangeColor()
        }
        return UIColor.grayColor()
    }
    
    // MARK: AlertController ActionSheet
    
    @IBAction func infoButtonPressed(sender: AnyObject) {
        let message = self.user.isSignedIn ? NSLocalizedString("You are sign in as \(self.user.name).", comment: "info message about user login state") : NSLocalizedString("You are not signed.", comment: "info message about user login state")
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: string_cancel, style: .Cancel) { (action) in }
        
        let signInTitle = self.user.isSignedIn ? NSLocalizedString("Sign out", comment: "button, signs out user") : string_signIn
        let loginAction = UIAlertAction(title: signInTitle, style: .Default) { (action) in
            if self.user.isSignedIn {
                self.signOut()
            } else {
                self.signIn()
            }
        }
        
        let mapString = self.mapView.styleURL == MGLStyle.streetsStyleURL() ? NSLocalizedString("Hybrid Map Style", comment: "style of map, combination of sattelite and street") : NSLocalizedString("Street Map Style", comment: "style of map, street")
        let mapStyleAction = UIAlertAction(title: mapString, style: .Default) { (action) -> Void in
            if self.mapView.styleURL == MGLStyle.streetsStyleURL() {
                self.mapView.styleURL = MGLStyle.hybridStyleURL()
            } else {
                self.mapView.styleURL = MGLStyle.streetsStyleURL()
            }
        }
        
        let tracksTitle =  NSLocalizedString("Recorded Tracks", comment: "title of button and Tracks scene")
        let tracksAction = UIAlertAction(title:tracksTitle, style: UIAlertActionStyle.Default) { (action) -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let tracksNavigationController = storyboard.instantiateViewControllerWithIdentifier("TracksNavigationController") as? UINavigationController where tracksNavigationController.topViewController is TracksViewController {
                tracksNavigationController.topViewController?.title = tracksTitle
                self.presentViewController(tracksNavigationController, animated: true, completion: nil)
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(loginAction)
        alertController.addAction(mapStyleAction)
        alertController.addAction(tracksAction)
        
        self.presentViewController(alertController, animated: true){}
        
    }
    
    func signIn(){
        let message = NSLocalizedString("Enter login name and password", comment: "message")
        let alertController = UIAlertController(title: string_signIn, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: string_cancel, style: .Cancel) { (action) in }
        
        let loginAction = UIAlertAction(title: string_signIn, style: .Default) { (_) in
            let loginTextField = alertController.textFields![0]
            let passwordTextField = alertController.textFields![1]
            
            self.login(loginTextField.text!, password: passwordTextField.text!)
        }
        loginAction.enabled = false
        
        let forgotPasswordAction = UIAlertAction(title: NSLocalizedString("Forgot Password", comment: "button title or placeholder or other text"), style: .Destructive) { (_) in }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = string_login
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                loginAction.enabled = textField.text != ""
            }
        }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = string_password
            textField.secureTextEntry = true
        }
        
        alertController.addAction(loginAction)
        alertController.addAction(forgotPasswordAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func login(loginName: String, password: String){
        //if it is possible login, login
        //otherwise show re-enter password
        
        self.user.login(loginName, password: password) { (success) -> Void in
            if success {
                //yeey!
            } else {
                let registerTitle = NSLocalizedString("Register", comment: "title")
                let signUpMessage = NSLocalizedString("Login name with entered password doesn't exist. Re-enter password to register or try again.", comment: "info text")
                let alertController = UIAlertController(title: registerTitle, message: signUpMessage, preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: string_cancel, style: .Cancel) { (action) in }
                
                let loginAction = UIAlertAction(title: string_login, style: .Default) { (_) in
                    let passwordTextField = alertController.textFields![0]
                    
                    self.login(loginName, password: passwordTextField.text!)
                }
                loginAction.enabled = false
                
                let signUpAction = UIAlertAction(title: registerTitle, style: .Default) { (action) in }
                
                let forgotPasswordAction = UIAlertAction(title: NSLocalizedString("Forgot Password", comment: "button title or placeholder or other text"), style: .Destructive) { (_) in }
                
                alertController.addTextFieldWithConfigurationHandler { (textField) in
                    textField.placeholder = string_password
                    textField.secureTextEntry = true
                    
                    NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                        loginAction.enabled = textField.text != ""
                    }
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(loginAction)
                alertController.addAction(signUpAction)
                alertController.addAction(forgotPasswordAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    func signOut(){
        self.user.isSignedIn = false
    }

}

