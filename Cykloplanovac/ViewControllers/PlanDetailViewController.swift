//
//  PlanDetailViewController.swift
//  Cykloplanovac
//
//  Created by Jenda on 30/11/15.
//  Copyright © 2015 FEL CVUT. All rights reserved.
//

import UIKit

protocol PlanDetailViewControllerDelegate {
    func planDetailViewControllerDidSelectPlan(plan:Plan?)
}

class PlanDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var planTitleLabel: UILabel!
    var delegate:PlanDetailViewControllerDelegate?
    var journey:Journey?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate?.planDetailViewControllerDidSelectPlan(journey?.plans?.firstObject as? Plan)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillLayoutSubviews() {
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let cells = self.collectionView.visibleCells()
        print("visible cells ",cells.count)
        if cells.count > 0 {
            if let indexPath = self.collectionView.indexPathForCell(cells[0]) {
                if let plan = journey?.plans?[indexPath.item] as? Plan, count = journey?.plans?.count {
                    self.planTitleLabel.text = NSLocalizedString("Plan \(indexPath.item+1) out of \(count) plans", comment: "text for selected plan")
                    self.delegate?.planDetailViewControllerDidSelectPlan(plan)
                }
            }
        }
    }
    
    //MARK: UICollectionView
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var size = self.collectionView.bounds.size
        size.width = size.width - 8
        return size
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.journey?.plans?.count {
            return count
        }
        return 1
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell:PlanDetailCollectionViewCell
        if self.journey?.plans?.count > 0 {
            let cellIdentifier = "PlanDetailCell"
            cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlanDetailCollectionViewCell
            
            if let travelTime = (journey?.plans?[indexPath.row] as? Plan)?.criteria?.travelTime {
                let dateFormater = NSDateComponentsFormatter()
                dateFormater.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute]
                dateFormater.unitsStyle = NSDateComponentsFormatterUnitsStyle.Short
                if let travelTimeString = dateFormater.stringFromTimeInterval(travelTime.doubleValue) {
                    cell.topLeftLabel.text =  travelTimeString
                }
            }
            if let stress = (journey?.plans?[indexPath.row] as? Plan)?.criteria?.stress {
                let stressString = NSLocalizedString("Stress", comment: "abrevient for Stress Unit")
                cell.topRightLabel.text = "\(stress) \(stressString)"
            }
            if let physicalEffort = (journey?.plans?[indexPath.row] as? Plan)?.criteria?.physicalEffort {
                let energyFormater = NSEnergyFormatter()
                let numberFormatter = NSNumberFormatter()
                numberFormatter.minimumFractionDigits = 0
                numberFormatter.maximumFractionDigits = 0
                energyFormater.numberFormatter = numberFormatter
                cell.bottomLeftLabel.text = energyFormater.stringFromJoules(physicalEffort.doubleValue)
            }
            if let distance = (journey?.plans?[indexPath.row] as? Plan)?.distance {
                let lenghtFormater = NSLengthFormatter()
                let numberFormatter = NSNumberFormatter()
                numberFormatter.minimumFractionDigits = 1
                numberFormatter.maximumFractionDigits = 1
                lenghtFormater.numberFormatter = numberFormatter
                cell.bottomRightLabel.text = lenghtFormater.stringFromMeters(distance.doubleValue)
            }
        } else {
            let cellIdentifier = "OopsPlanDetailCell"
            cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlanDetailCollectionViewCell
        }
        
        return cell
    }
}
